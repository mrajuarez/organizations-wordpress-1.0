<?php
/**
 Plugin Name: Organizations of Galatea
 Version: 1.0
 Description: Organizations of Galatea. This plugin create an option page callable from the WordPress Management module and where is managed the Organizations catalog of Galatea's project under or through which the Organization operates.
 Author: Mario Rauz Alejandro Juárez Gutiérrez
 Author URI: mailto:mrajuarez@gmail.com
 Plugin URI: https://github.com/mrajuarez/organizations-wordpress-1.0
 Text Domain: organizations
 Domain Path: /languages
 License: GNU GPLv3
 License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 
 Galatea Copyright (C) 2015-2018 Mario Rauz Alejandro Juárez Gutiérrez
 This program comes with ABSOLUTELY NO WARRANTY.
 This is free software, and you are welcome to redistribute it under certain conditions.
 
 Copyright (C) 2015-2018 Ing. Mario Rauz Alejandro Juárez Gutiérerez
 
 This program is free software: you can redistribute it and/or modify it under the terms of the GNU
 General Public License as published by the Free Software Foundation, either version 3 of the
 License, or(at your option) any later version.
 
 This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 
 You should have received a copy of the GNU General Public License along with this program. If not,
 see <https://www.gnu.org/licenses/>.
 
 @package    Galatea
 @version    1.0, 11/Feb/2018
 @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
 @copyright  Copyright (C) 2015-2018 Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
 @license    GPLv3
 */

// Avoid direct calls to this file
if(!function_exists('add_action')) {
  header('Status: 403 Forbidden');
  header('HTTP/1.1 403 Forbidden');
  exit();
} // if(!function_exists('add_action'))


// Install this plugin only for users with the 'administrator' role
if(is_admin()) {
  // Define the function only if this doesn't exists
  if(!function_exists('process_organizations_callback')) {
    /**
     Process a page callback.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function process_organizations_callback() {
      // If the current user wants to delete a single record
      if($_REQUEST['action'] == 'delete')
        delete_organizations(array($_REQUEST['organization']));
      // If the current user wants to delete a set of records
      elseif($_REQUEST['action_top'] == 'delete_selected' || $_REQUEST['action_bottom'] == 'delete_selected')
        delete_organizations($_REQUEST['organizations']);
      // If the current user wants to empty the trash bin
      elseif($_REQUEST['action'] == 'empty')
        empty_trash_organizations();
      // If the current user wants to delete a single record
      elseif($_REQUEST['action'] == 'restore')
        restore_organizations(array($_REQUEST['organization']));
      // If the current user wants to delete a set of records
      elseif($_REQUEST['action_top'] == 'restore_selected' || $_REQUEST['action_bottom'] == 'restore_selected')
        restore_organizations($_REQUEST['organizations']);
      // If the current user wants to remove a logo
      elseif($_REQUEST['action'] == 'remove_logo')
        remove_logo();
      // If the current user wants to remove a template
      elseif($_REQUEST['action'] == 'remove_template')
        remove_template();
      // If the current user wants to save a single record
      elseif($_REQUEST['action'] == 'save')
        save_organization();
      // If the current user wants to upload the Organizations list
      elseif($_REQUEST['action'] == 'upload')
        upload_organizations();
      
      // If the current user wants to edit a record
      if($_REQUEST['action'] == 'edit')
        edit_organization();
      // Otherwise prints the Organization list
      else
        list_organizations();
      return;
    } // function process_organizations_callback( ...
  } // if(!function_exists('process_organizations_callback'))

  // Define the function only if this doesn't exists
  if(!function_exists('add_organizations_menu')) {
    /**
     Add the options page in WordPress.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function add_organizations_menu() {
      // Loads the localisation configuration files
      if(file_exists(plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . get_locale() . '.mo'))
        load_textdomain('organizations', plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . get_locale() . '.mo');
      elseif(file_exists(plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . substr(get_locale(), 0, 2) . '.mo'))
        load_textdomain('organizations', plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . substr(get_locale(), 0, 2) . '.mo');
      load_plugin_textdomain('organizations',  false, plugin_basename(dirname(__FILE__)) . '/languages/');

      // This page will be under "Settings"
      add_options_page(
          __('Organizations', 'organizations'), 
          __('Organizations', 'organizations'), 
          'manage_options', 
          'organizations', 
          'process_organizations_callback'
     );
      return;
    } // function add_organizations_menu( ...
    add_action('admin_menu', 'add_organizations_menu');
  } // if(!function_exists('add_organizations_menu'))

  // Define the function only if this doesn't exists
  if(!function_exists('add_organizations_menu_help')) {
    add_action('admin_head', 'add_organizations_menu_help');
    /**
     Add help contextual menu, command and content in WordPress.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function add_organizations_menu_help() {
      // While this is the current page
      if($_REQUEST['page'] == 'organizations') {
        // Get the current screen
        $current_screen = get_current_screen();
        
        // Create the primary help tab
        $current_screen->add_help_tab(array(
            'id'      => 'help_tab',
            'title'   => __('Help', 'organizations'),
            'callback' => 'help_tab_organizations',));

        // Create the secondary help tab
        $current_screen->add_help_tab(array(
            'id'       => 'toubleshoting_tab',
            'title'    => __('Troubleshoting', 'organizations'),
            'callback' => 'troubleshoting_tab_organizations',));

        // Create the help sidebar
        $current_screen->set_help_sidebar(
            '<p>
               <strong>
                 ' . esc_html__('For more information', 'organizations') . ':
               </strong>
             </p>
             <p>
               <a href="' . esc_attr__('https://github.com/mrajuarez/organizations-wordpress-1.0', 'organizations') . '">
                 ' . esc_html__('Official documentation', 'organizations') . '
               </a>
             </p>
             <p>
               <a href="' . esc_attr__('https://242.mx/support', 'organizations') . '">
                 ' . esc_html__('Technical sopport forum', 'organizations') . '
               </a>
             </p>');

        // Create the screen options tab
        if($_REQUEST['action'] != 'edit')
          add_screen_option(
            'per_page',
            array('label'   => __('Records per page', 'organizations'),
                  'default' => intval($_REQUEST['per_page'])));
      } // if($_REQUEST['page'] == 'organizations')
      return;
    } // function add_organizations_menu_help( ...
  } // if(!function_exists('add_organizations_menu_help'))

  // Define the function only if this doesn't exists
  if(!function_exists('delete_organizations')) {
    /**
     Delete a set of records.
     
     @param      array  $organizations  Organizations emails list
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function delete_organizations($organizations) {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      // Delete the selected records
      for($i = 0; $i < count($organizations); $i++) {
        $sql = 'UPDATE gt_organizations SET
                  deleted = 1,
                  editor  = "' . $current_user . '",
                  edition = DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")
                WHERE organization = "' . $organizations[$i] . '"';
        $wpdb->query($sql);
        $sql = 'INSERT INTO gt_logs VALUES(
                  "' . $current_user . '",
                  DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                  "event=The selected organizations have been successfully deleted.' .
                 '&organization=' . $organizations[$i] . '")';
        $wpdb->query ($sql);
      } // for($i = 0; $i < count($organizations); $i++)

      // Print a message
      echo '<div id="message" class="updated notice is-dismissible">' .
             '<p>' . esc_html__('The selected organizations have been successfully deleted.', 'organizations') . '</p>' .
           '</div>';
      return;
    } // function delete_organization( ...
  } // if(!function_exists('delete_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('download_organizations')) {
    add_action('wp_ajax_download_organizations', 'download_organizations');
    /**
     Download the current records.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function download_organizations() {
      // Loads the plugin utilities library
      include_once(ABSPATH . '/wp-admin/includes/plugin.php');

      // Protect against unauthorized web entries
      if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php') || !wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        header('Status: 403 Forbidden');
        header('HTTP/1.1 403 Forbidden');
        wp_die();
      } // if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php') || !wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Create and open a new spreadsheet
      $spreadsheet = new Spreadsheet();
      $spreadsheet->getProperties()->setCreator('Galatea (' . __('https://github.com/mrajuarez/organizations-wordpress-1.0', 'organizations') . ')')
                  ->setLastModifiedBy('Galatea (' . __('https://github.com/mrajuarez/organizations-wordpress-1.0', 'organizations') . ')')
                  ->setTitle(__('Organizations', 'organizations'))
                  ->setSubject(__('Organizations', 'organizations'))
                  ->setDescription(__('Organizations list for Office 2007 XLSX', 'organizations'))
                  ->setKeywords('office 2007 openxml php')
                  ->setCategory('Galatea');
      $sheet = $spreadsheet->getActiveSheet();
      $sheet->setTitle(__('Organizations', 'organizations'));
      $sheet->setShowGridlines(false);

      // Set up header columns
      $sheet->setCellValue('A1',  'organization')
            ->setCellValue('B1',  'name')
            ->setCellValue('C1',  'description')
            ->setCellValue('D1',  'main_street')
            ->setCellValue('E1',  'main_outside')
            ->setCellValue('F1',  'main_inside')
            ->setCellValue('G1',  'main_reference')
            ->setCellValue('H1',  'main_settlement')
            ->setCellValue('I1',  'main_postal_code')
            ->setCellValue('J1',  'main_county')
            ->setCellValue('K1',  'main_state')
            ->setCellValue('L1',  'main_country')
            ->setCellValue('M1',  'tax_name')
            ->setCellValue('N1',  'tax_id')
            ->setCellValue('O1',  'tax_regime')
            ->setCellValue('P1',  'serial')
            ->setCellValue('Q1',  'currency')
            ->setCellValue('R1',  'way_to_pay')
            ->setCellValue('S1',  'payment_method')
            ->setCellValue('T1',  'use_of_voucher')
            ->setCellValue('U1',  'certificate_number')
            ->setCellValue('V1',  'einvoicing_url')
            ->setCellValue('W1',  'einvoicing_username')
            ->setCellValue('X1',  'einvoicing_password')
            ->setCellValue('Y1',  'einvoicing_service')
            ->setCellValue('Z1',  'tax_street')
            ->setCellValue('AA1', 'tax_outside')
            ->setCellValue('AB1', 'tax_inside')
            ->setCellValue('AC1', 'tax_reference')
            ->setCellValue('AD1', 'tax_settlement')
            ->setCellValue('AE1', 'tax_postal_code')
            ->setCellValue('AF1', 'tax_county')
            ->setCellValue('AG1', 'tax_state')
            ->setCellValue('AH1', 'tax_country')
            ->setCellValue('AI1', 'phone_1')
            ->setCellValue('AJ1', 'phone_2')
            ->setCellValue('AK1', 'phone_3')
            ->setCellValue('AL1', 'mobile')
            ->setCellValue('AM1', 'fax');
      
      // Download the list of records
      global $wpdb;
      $sql = 'SELECT organization,
                     name,
                     description,
                     main_street,
                     main_outside,
                     main_inside,
                     main_reference,
                     main_settlement,
                     main_postal_code,
                     main_county,
                     main_state,
                     main_country,
                     tax_name,
                     tax_id,
                     tax_regime,
                     serial,
                     currency,
                     way_to_pay,
                     payment_method,
                     use_of_voucher,
                     certificate_number,
                     einvoicing_url,
                     einvoicing_username,
                     einvoicing_password,
                     einvoicing_service,
                     tax_street,
                     tax_outside,
                     tax_inside,
                     tax_reference,
                     tax_settlement,
                     tax_postal_code,
                     tax_county,
                     tax_state,
                     tax_country,
                     phone_1,
                     phone_2,
                     phone_3,
                     mobile,
                     fax
              FROM gt_organizations
              WHERE deleted = 0
              ORDER BY organization';
      $results = $wpdb->get_results($sql);
      $r = 2;
      foreach($results as $row) {
        $sheet->setCellValue('A' .  $r, $row->organization)
              ->setCellValue('B' .  $r, $row->name)
              ->setCellValue('C' .  $r, $row->description)
              ->setCellValue('D' .  $r, $row->main_street)
              ->setCellValue('E' .  $r, $row->main_outside)
              ->setCellValue('F' .  $r, $row->main_inside)
              ->setCellValue('G' .  $r, $row->main_reference)
              ->setCellValue('H' .  $r, $row->main_settlement)
              ->setCellValue('I' .  $r, $row->main_postal_code)
              ->setCellValue('J' .  $r, $row->main_county)
              ->setCellValue('K' .  $r, $row->main_state)
              ->setCellValue('L' .  $r, $row->main_country)
              ->setCellValue('M' .  $r, $row->tax_name)
              ->setCellValue('N' .  $r, $row->tax_id)
              ->setCellValue('O' .  $r, $row->tax_regime)
              ->setCellValue('P' .  $r, $row->serial)
              ->setCellValue('Q' .  $r, $row->currency)
              ->setCellValue('R' .  $r, $row->way_to_pay)
              ->setCellValue('S' .  $r, $row->payment_method)
              ->setCellValue('T' .  $r, $row->use_of_voucher)
              ->setCellValue('U' .  $r, $row->certificate_number)
              ->setCellValue('V' .  $r, $row->einvoicing_url)
              ->setCellValue('W' .  $r, $row->einvoicing_username)
              ->setCellValue('X' .  $r, $row->einvoicing_password)
              ->setCellValue('Y' .  $r, $row->einvoicing_service)
              ->setCellValue('Z' .  $r, $row->tax_street)
              ->setCellValue('AA' . $r, $row->tax_outside)
              ->setCellValue('AB' . $r, $row->tax_inside)
              ->setCellValue('AC' . $r, $row->tax_reference)
              ->setCellValue('AD' . $r, $row->tax_settlement)
              ->setCellValue('AE' . $r, $row->tax_postal_code)
              ->setCellValue('AF' . $r, $row->tax_county)
              ->setCellValue('AG' . $r, $row->tax_state)
              ->setCellValue('AH' . $r, $row->tax_country)
              ->setCellValue('AI' . $r, $row->phone_1)
              ->setCellValue('AJ' . $r, $row->phone_2)
              ->setCellValue('AK' . $r, $row->phone_3)
              ->setCellValue('AL' . $r, $row->mobile)
              ->setCellValue('AM' . $r, $row->fax);
        $r++;
      } // foreach($results as $row)

      // Redirect output to a client’s web browser (Xls)
      header('Content-Type: application/vnd.ms-excel');
      header('Content-Disposition: attachment;' .
             'filename="' . __('Organizations', 'organizations') . '.xlsx"');
      header('Cache-Control: max-age=0');
      
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');
      
      // If you're serving to IE over SSL, then the following may be needed
      header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
      header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header('Pragma: public'); // HTTP/1.0

      // Create a content writer
      $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
      $writer->save('php://output');
      unset($writer);
      unset($spreadsheet);
      $sql = 'INSERT INTO gt_logs VALUES(
                "' . $current_user . '",
                DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                "event=Downloading the current registered organizations.")';
      $wpdb->query($sql);
      wp_die();
    } // function download_organizations( ...
    $recover = __('Downloading the current registered organizations.', 'organizations');
  } // if(!function_exists('download_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('empty_trash_organizations')) {
    /**
     Empty the trash bin.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function empty_trash_organizations() {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
            
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      // Delete all previous deleted records
      $sql = 'DELETE FROM gt_organizations
              WHERE deleted = 1';
      $wpdb->query($sql);
      $sql = 'INSERT INTO gt_logs VALUES(
                "' . $current_user . '",
                DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                "event=The trash bin is empty.")';
      $wpdb->query ($sql);
      echo '<div id="message" class="updated notice is-dismissible">' .
             '<p>' . esc_html__('The trash bin is empty.', 'organizations') . '</p>' .
           '</div>';
      return;
    } // function empty_trash_organizations( ...
  } // if(!function_exists('empty_trash_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('edit_organization')) {
    /**
     Print the form for edit a record.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function edit_organization() {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current record
      $sql = 'SELECT organization,
                     name,
                     description,
                     main_street,
                     main_outside,
                     main_inside,
                     main_reference,
                     main_settlement,
                     main_postal_code,
                     main_county,
                     main_state,
                     main_country,
                     tax_name,
                     tax_id,
                     tax_regime,
                     serial,
                     currency,
                     way_to_pay,
                     payment_method,
                     use_of_voucher,
                     certificate_number,
                     einvoicing_url,
                     einvoicing_username,
                     einvoicing_password,
                     einvoicing_service,
                     tax_street,
                     tax_outside,
                     tax_inside,
                     tax_reference,
                     tax_settlement,
                     tax_postal_code,
                     tax_county,
                     tax_state,
                     tax_country,
                     phone_1,
                     phone_2,
                     phone_3,
                     mobile,
                     fax,
                     logo,
                     template,
                     deleted,
                     editor,
                     edition
              FROM gt_organizations
              WHERE organization = "' . $_REQUEST['organization'] . '"';
      if($_REQUEST['paged'] < 1)
        $_REQUEST['paged'] = 1;
      if($_REQUEST['deleted'] != 0 && $_REQUEST['deleted'] != 1)
        $_REQUEST['deleted'] = 0;
      if($_REQUEST['per_page'] < 20)
        $_REQUEST['per_page'] = 20;
      $row = $wpdb->get_row($sql, ARRAY_A);
      if($wpdb->num_rows == 0 && !is_null($row)) {
        // Get the current user's email address
        $current_user = wp_get_current_user();
        $current_user = $current_user->user_email;

        // Init a row with default values
        $row = array('organization'        => '',
                     'name'                => '',
                     'description'         => '',
                     'main_street'         => '',
                     'main_outside'        => '',
                     'main_inside'         => '',
                     'main_reference'      => '',
                     'main_settlement'     => '',
                     'main_postal_code'    => '',
                     'main_county'         => '',
                     'main_state'          => '',
                     'main_country'        => '',
                     'tax_name'            => '',
                     'tax_id'              => '',
                     'tax_regime'          => '',
                     'serial'              => '',
                     'currency'            => '',
                     'way_to_pay'          => '',
                     'payment_method'      => '',
                     'use_of_voucher'      => '',
                     'certificate_number'  => '',
                     'einvoicing_url'      => '',
                     'einvoicing_username' => '',
                     'einvoicing_password' => '',
                     'einvoicing_service'  => '',
                     'tax_street'          => '',
                     'tax_outside'         => '',
                     'tax_inside'          => '',
                     'tax_reference'       => '',
                     'tax_settlement'      => '',
                     'tax_postal_code'     => '',
                     'tax_county'          => '',
                     'tax_state'           => '',
                     'tax_country'         => '',
                     'phone_1'             => '',
                     'phone_2'             => '',
                     'phone_3'             => '',
                     'mobile'              => '',
                     'fax'                 => '',
                     'logo'                => 0,
                     'template'            => 0);
      }
      ?>
      <div class="wrap">
        <h1 class="wp-heading-inline">
          <?php esc_html_e('Organization', 'organizations') ?>
        </h1>
        <hr class="wp-header-end" />
        <form action="/wp-admin/options-general.php" method="post" novalidate="novalidate">
          <input type="hidden" name="page" value="organizations" />
          <input type="hidden" name="action" value="save" />
          <input type="hidden" name="paged" value="<?php echo esc_attr($_REQUEST['paged']) ?>" />
          <input type="hidden" name="deleted" value="<?php echo esc_attr($_REQUEST['deleted']) ?>" />
          <input type="hidden" name="per_page" value="<?php echo esc_attr($_REQUEST['per_page']) ?>" />
          <?php wp_nonce_field('organizations-nonce') ?>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="organization">
                  <?php esc_html_e('Email', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="organization" name="organization" type="text" maxlength="255" aria-describedby="organization-description" class="regular-text" value="<?php echo esc_attr($row['organization']) ?>" />
                <p class="description" id="organization-description">
                  <?php esc_html_e('Generally it is the main email address used officially by this organization.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="name">
                  <?php esc_html_e('Name', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="name" name="name" type="text" maxlength="100" aria-describedby="tax_name-description" class="regular-text" value="<?php echo esc_attr($row['name']) ?>" />
                <p class="description" id="name-description">
                  <?php esc_html_e('It is the common name used by the organization. It could be a legal, commercial or a simple name used by public in general.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="description">
                  <?php esc_html_e('Description', 'organizations') ?>:
                </label>
              </th>
              <td>
                <textarea id="description" name="description" aria-describedby="description-description" class="regular-text"><?php echo esc_textarea($row['description']) ?></textarea>
                <p class="description" id="description-description">
                  <?php esc_html_e('A brief description about this organization. It could be a short desciption about organization, its mission and vision, main activities and services.', 'organizations') ?>
                </p>
              </td>
            </tr>
          </table>
          <h1>
            <?php esc_html_e('Main residence', 'organizations') ?>
          </h1>
          <script type="text/javascript">
          /**
           Update the selected main state, county and settlement inside of its own combo boxes from the current main country and postal code.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_main_address() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=address" +
                        "&country=" + document.getElementById("main_country").value +
                        "&postal_code=" + document.getElementById("main_postal_code").value;
              jQuery.get(url, function(response) {
                var address = JSON.parse(JSON.stringify(response));
                if(address != null) {
                  if(address.length > 0) {
                    var combo = document.getElementById("main_state");
                    for(var i = 0; i < combo.length; i++) {
                      if(address[0].state == combo.options[i].value) {
                        combo.selectedIndex = i;
                        i = combo.length;
                      } // if(address[0].state == combo.options[i].value)
                    } // for(var i = 0; i < combo.length; i++)
                    var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                              "?action=update_postal_codes" +
                              "&code=counties" +
                              "&country=" + document.getElementById("main_country").value +
                              "&state=" + address[0].state;
                    jQuery.get(url, function(response) {
                      var combo = document.getElementById("main_county");
                      combo.options.length = 0;
                      combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, address[0].county == '');
                      combo.options[0].disabled = true;
                      var catalog = JSON.parse(JSON.stringify(response));
                      if(catalog != null)
                        for(var i = 0; i < catalog.length; i++)
                          combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, address[0].county == catalog[i].key);
                    }); // jQuery.get(url, function(response)
                    var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                              "?action=update_postal_codes" +
                              "&code=settlements" +
                              "&country=" + document.getElementById("main_country").value +
                              "&state=" + address[0].state +
                              "&county=" + address[0].county;
                    jQuery.get(url, function(response) {
                      var combo = document.getElementById("main_settlement");
                      combo.options.length = 0;
                      combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, address[0].settlement == '');
                      combo.options[0].disabled = true;
                      var catalog = JSON.parse(JSON.stringify(response));
                      if(catalog != null)
                        for(var i = 0; i < catalog.length; i++)
                          combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, address[0].settlement == catalog[i].key);
                    }); // jQuery.get(url, function(response)
                  } // if(address.length > 0)
                } // if(address != null)
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_main_address( ...

          /**
           Update the current main postal code inside of its own text box from the current main country, state, county and settlement.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_main_postal_code() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=postal_code" +
                        "&country=" + document.getElementById("main_country").value +
                        "&state=" + document.getElementById("main_state").value +
                        "&county=" + document.getElementById("main_county").value +
                        "&settlement=" + document.getElementById("main_settlement").value;
              jQuery.get(url, function(response) {
                var textbox = document.getElementById("main_postal_code");
                var value = JSON.parse(JSON.stringify(response));
                if(value == null)
                  textbox.value = "";
                else if(value.length < 1)
                  textbox.value = "";
                else
                  textbox.value = value[0].value;
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_main_postal_code( ...

          /**
           Update the available main settlements inside of its own combo box from the current main country, state and county.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_main_settlements() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=settlements" +
                        "&country=" + document.getElementById("main_country").value +
                        "&state=" + document.getElementById("main_state").value +
                        "&county=" + document.getElementById("main_county").value;
              jQuery.get(url, function(response) {
                var combo = document.getElementById("main_settlement");
                var current = combo.options[combo.selectedIndex];
                combo.options.length = 0;
                combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, current == '');
                combo.options[0].disabled = true;
                var catalog = JSON.parse(JSON.stringify(response));
                if(catalog != null)
                  for(var i = 0; i < catalog.length; i++)
                    combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, catalog[i].key == current);
                update_main_postal_code();
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_main_settlements( ...

          /**
           Update the available main counties inside of its own combo box from the current main
           country and state.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_main_counties() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=counties" +
                        "&country=" + document.getElementById("main_country").value +
                        "&state=" + document.getElementById("main_state").value;
              jQuery.get(url, function(response) {
                var combo = document.getElementById("main_county");
                var current = combo.options[combo.selectedIndex];
                combo.options.length = 0;
                combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, current == '');
                combo.options[0].disabled = true;
                var catalog = JSON.parse(JSON.stringify(response));
                if(catalog != null)
                  for(var i = 0; i < catalog.length; i++)
                    combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, catalog[i].key == current);
                update_main_settlements();
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_main_counties( ...

          /**
           Update the available main states inside of its own combo box from the current main country.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_main_states() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=states" +
                        "&country=" + document.getElementById("main_country").value;
              jQuery.get(url, function(response) {
                var combo = document.getElementById("main_state");
                var current = combo.options[combo.selectedIndex];
                combo.options.length = 0;
                combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, current == '');
                combo.options[0].disabled = true;
                var catalog = JSON.parse(JSON.stringify(response));
                if(catalog != null)
                  for(var i = 0; i < catalog.length; i++)
                    combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, catalog[i].key == current);
                update_main_counties();
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_main_states( ...
          </script>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="main_country">
                  <?php esc_html_e('Country', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="main_country" name="main_country" onChange="update_main_states();">
                  <option value="" disabled<?php echo($row['main_country'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT country AS country 
                        FROM gt_settlements
                        ORDER BY country';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->country) ?>"<?php echo($catalog->country == $row['main_country'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->country) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_state">
                  <?php esc_html_e('State, department or province', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="main_state" name="main_state" onChange="update_main_counties();">
                  <option value="" disabled<?php echo($row['main_state'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT state AS state
                        FROM gt_settlements
                        WHERE country = "' . $row['main_country'] . '"
                        ORDER BY state';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->state) ?>"<?php echo($catalog->state == $row['main_state'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->state) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_county">
                  <?php esc_html_e('Municipality, delegation or county', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="main_county" name="main_county" onChange="update_main_settlements();">
                  <option value="" disabled<?php echo($row['main_county'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT county AS county
                        FROM gt_settlements
                        WHERE country = "' . $row['main_country'] . '" AND
                              state   = "' . $row['main_state'] . '"
                       ORDER BY county';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->county) ?>"<?php echo($catalog->county == $row['main_county'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->county) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_settlement">
                  <?php esc_html_e('Settlement', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="main_settlement" name="main_settlement" onChange="update_main_postal_code();">
                  <option value="" disabled<?php echo($row['main_settlement'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT settlement AS settlement
                        FROM gt_settlements
                        WHERE country = "' . $row['main_country'] . '" AND
                              state = "' . $row['main_state'] . '" AND
                              county = "' . $row['main_county'] . '"
                        ORDER BY settlement';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->settlement) ?>"<?php echo($catalog->settlement == $row['main_settlement'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->settlement) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_postal_code">
                  <?php esc_html_e('Postal code', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="main_postal_code" name="main_postal_code" type="number" min="1" max="99999" step="1" maxlength="5" aria-describedby="main_postal_code-description" class="regular-text" value="<?php echo esc_attr($row['main_postal_code']) ?>" onChange="update_main_address();" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_street">
                  <?php esc_html_e('Street', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="main_street" name="main_street" type="text" maxlength="100" aria-describedby="main_street-description" class="regular-text" value="<?php echo esc_attr($row['main_street']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_outside">
                  <?php esc_html_e('Exterior number', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="main_outside" name="main_outside" type="text" maxlength="30" aria-describedby="main_outside-description" class="regular-text" value="<?php echo esc_attr($row['main_outside']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_inside">
                  <?php esc_html_e('Interior number', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="main_inside" name="main_inside" type="text" maxlength="30" aria-describedby="main_inside-description" class="regular-text" value="<?php echo esc_attr($row['main_inside']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="main_reference">
                  <?php esc_html_e('Reference', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="main_reference" name="main_reference" type="text" maxlength="100" aria-describedby="main_reference-description" class="regular-text" value="<?php echo esc_attr($row['main_reference']) ?>" />
              </td>
            </tr>
          </table>
          <h1><?php esc_html_e('Taxpayer registration data', 'organizations') ?></h1>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="tax_name">
                  <?php esc_html_e('Federal taxpayer name', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_name" name="tax_name" type="text" maxlength="255" aria-describedby="tax_name-description" class="regular-text" value="<?php echo esc_attr($row['tax_name']) ?>" />
                <p class="description" id="tax_name-description">
                  <?php esc_html_e('It is the legal name of organization. Commonly registered with government authorities.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_id">
                  <?php esc_html_e('Federal taxpayer registration ID', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_id" name="tax_id" type="text" maxlength="13" aria-describedby="tax_id-description" class="regular-text" value="<?php echo esc_attr($row['tax_id']) ?>" />
                <p class="description" id="tax_id-description">
                  <?php esc_html_e('It is the legal identifier number of organization. Commonly registered and assigned with and by government authorities.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_regime">
                  <?php esc_html_e('Taxpayer regime', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="tax_regime" name="tax_regime">
                  <option value="" disabled<?php echo($row['tax_regime'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT code,
                               name
                        FROM gt_tax_regimes
                        ORDER BY name';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->code) ?>"<?php echo($catalog->code == $row['tax_regime'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->name) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
                <p class="description" id="tax_regime-description">
                  <?php esc_html_e('It is the taxpayer regime where organization assigned by government authorities. This param is included inside of all inssued invoices.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="serial">
                  <?php esc_html_e('Serial', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="serial" name="serial" type="text" maxlength="25"
                       aria-describedby="serial-description" class="regular-text"
                       value="<?php echo esc_attr($row['serial']) ?>" />
                <p class="description" id="serial-description">
                  <?php esc_html_e('It is a serial and alphanumeric number for all invoices issued by this organization. It must be a unique and distinct to other serial numbers used inside of other information system and with and by this organization.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="currency">
                  <?php esc_html_e('Currency', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="currency" name="currency">
                  <option value="" disabled<?php echo($row['currency'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT code,
                               name
                        FROM gt_currency_codes
                        ORDER BY name';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->code) ?>"<?php echo($catalog->code == $row['currency'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->name) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
                <p class="description" id="currency-description">
                  <?php esc_html_e('It is the currency used for all invoices issued by this organization. It must be a valid <a href="https://es.wikipedia.org/wiki/ISO_4217">ISO 4217</a> and registered currency inside of Galatea\'s currencies catalog.', 'organizations') ?>
                </p>
              </td>
            </tr>       
            <tr>
              <th scope="row">
                <label for="way_to_pay">
                  <?php esc_html_e('Way to pay', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="way_to_pay" name="way_to_pay">
                  <option value="" disabled<?php echo($row['way_to_pay'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT code,
                               name
                        FROM gt_ways_to_pay
                        ORDER BY name';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->code) ?>"<?php echo($catalog->code == $row['way_to_pay'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->name) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
                <p class="description" id="way_to_pay-description">
                  <?php esc_html_e('It is the way to pay used for all customers inside of your OnLine Web Store and which invoices are issued by this organization. It must be a valid way to pay authorized and registered by government authorities and inside of Galatea\'s ways to pay catalog.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="payment_method">
                  <?php esc_html_e('Payment method', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="payment_method" name="payment_method">
                  <option value="" disabled<?php echo($row['payment_method'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT code,
                               name
                        FROM gt_payment_methods
                        ORDER BY name';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->code) ?>"<?php echo($catalog->code == $row['payment_method'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->name) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
                <p class="description" id="payment_method-description">
                  <?php esc_html_e('It is the payment method used for all customers inside of your OnLine Web Store and which invoices are issued by this organization. It must be a valid payment method authorized and registered by government authorities and inside of Galatea\'s payment methods catalog.', 'organizations') ?>
                </p>
              </td>
            </tr>
            
            <tr>
              <th scope="row">
                <label for="use_of_voucher">
                  <?php esc_html_e('Use of voucher', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="use_of_voucher" name="use_of_voucher">
                  <option value="" disabled<?php echo($row['use_of_voucher'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT code,
                               name
                        FROM gt_uses_of_vouchers
                        ORDER BY name';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->code) ?>"<?php echo($catalog->code == $row['use_of_voucher'] ? 'selected' : '') ?>>
                    <?php echo $catalog->name ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
                <p class="description" id="use_of_voucher-description">
                  <?php esc_html_e('It is the use of vouchers established for all invoices issued by this organization. It must be a valid use of voucher authorized and registered by government authorities and inside of Galatea\'s uses of vouchers catalog.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="certificate_number">
                  <?php esc_html_e('Certificate number', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="certificate_number" name="certificate_number" type="text" maxlength="255" aria-describedby="certificate_number-description" class="regular-text" value="<?php echo esc_attr($row['certificate_number']) ?>" />
                <p class="description" id="certificate_number-description">
                  <?php esc_html_e('It is the number of certificate established for all invoices issued by this organization. It must be a valid certificate number authorized and registered by government authorities.', 'organizations') ?>
                </p>
              </td>
            </tr>
          </table>
          <h1>
            <?php esc_html_e('Electronic invoicing service', 'organizations') ?>
          </h1>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="einvoicing_url">
                  <?php esc_html_e('URL', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="einvoicing_url" name="einvoicing_url" type="url" maxlength="255" aria-describedby="einvoicing_url-description" class="regular-text" value="<?php echo esc_attr($row['einvoicing_url']) ?>" />
                <p class="description" id="einvoicing_url-description">
                  <?php esc_html_e('It is the URL of electronic invoicing services of facturehoy.com. This param it must be assigned by your stamping and sealing for electronic invoice services provider.', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="einvoicing_username">
                  <?php esc_html_e('Username', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="einvoicing_username" name="einvoicing_username" type="text" maxlength="255" aria-describedby="einvoicing_username-description" class="regular-text" value="<?php echo esc_attr($row['einvoicing_username']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="einvoicing_password">
                  <?php esc_html_e('Password', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="einvoicing_password" name="einvoicing_password" type="password" maxlength="255" aria-describedby="tax_id-description" class="regular-text" value="<?php echo esc_attr($row['einvoicing_password']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="einvoicing_service">
                  <?php esc_html_e('Electronic invoicing service', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="einvoicing_service" name="einvoicing_service">
                  <option value="" disabled<?php echo($row['einvoicing_service'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option>
                  <option value="5906390"<?php echo($row['einvoicing_service'] == '5906390' ? ' selected' : '') ?>>
                    <?php esc_html_e('Only stamped', 'organizations') ?>
                  </option>
                  <option value="36424534"<?php echo($row['einvoicing_service'] == '36424534' ? ' selected' : '') ?>>
                    <?php esc_html_e('Stamped and sealing', 'organizations') ?>
                  </option>
                </select>
              </td>
            </tr>
          </table>
          <h1>
            <?php esc_html_e('Tax residence', 'organizations') ?>
          </h1>
          <script type="text/javascript">
          /**
           Update the selected tax state, county and settlement inside of its own combo boxes from the current tax country and
           postal code.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_tax_address() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=address" +
                        "&country=" + document.getElementById("tax_country").value +
                        "&postal_code=" + document.getElementById("tax_postal_code").value;
              jQuery.get(url, function(response) {
                var address = JSON.parse(JSON.stringify(response));
                if(address != null) {
                  if(address.length > 0) {
                    var combo = document.getElementById("tax_state");
                    for(var i = 0; i < combo.length; i++) {
                      if(address[0].state == combo.options[i].value) {
                        combo.selectedIndex = i;
                        i = combo.length;
                      } // if(address[0].state == combo.options[i].value)
                    } // for(var i = 0; i < combo.length; i++)
                    var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                              "?action=update_postal_codes" +
                              "&code=counties" +
                              "&country=" + document.getElementById("tax_country").value +
                              "&state=" + address[0].state;
                    jQuery.get(url, function(response) {
                      var combo = document.getElementById("tax_county");
                      combo.options.length = 0;
                      combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, address[0].county == '');
                      combo.options[0].disabled = true;
                      var catalog = JSON.parse(JSON.stringify(response));
                      if(catalog != null)
                        for(var i = 0; i < catalog.length; i++)
                          combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, address[0].county == catalog[i].key);
                    }); // jQuery.get(url, function(response)
                    var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                              "?action=update_postal_codes" +
                              "&code=settlements" +
                              "&country=" + document.getElementById("tax_country").value +
                              "&state=" + address[0].state +
                              "&county=" + address[0].county;
                    jQuery.get(url, function(response) {
                      var combo = document.getElementById("tax_settlement");
                      combo.options.length = 0;
                      combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, address[0].settlement == '');
                      combo.options[0].disabled = true;
                      var catalog = JSON.parse(JSON.stringify(response));
                      if(catalog != null)
                        for(var i = 0; i < catalog.length; i++)
                          combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, address[0].settlement == catalog[i].key);
                    }); // jQuery.get(url, function(response)
                  } // if(address.length > 0)
                } // if(address != null)
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_tax_address( ...

          /**
           Update the current tax postal code inside of its own text box from the current tax country, state, county and settlement.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_tax_postal_code() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=postal_code" +
                        "&country=" + document.getElementById("tax_country").value +
                        "&state=" + document.getElementById("tax_state").value +
                        "&county=" + document.getElementById("tax_county").value +
                        "&settlement=" + document.getElementById("tax_settlement").value;
              jQuery.get(url, function(response) {
                var textbox = document.getElementById("tax_postal_code");
                var value = JSON.parse(JSON.stringify(response));
                if(value == null)
                  textbox.value = "";
                else if(value.length < 1)
                  textbox.value = "";
                else
                  textbox.value = value[0].value;
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_tax_postal_code( ...

          /**
           Update the available tax settlements inside of its own combo box from the current tax country, state and county.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_tax_settlements() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=settlements" +
                        "&country=" + document.getElementById("tax_country").value +
                        "&state=" + document.getElementById("tax_state").value +
                        "&county=" + document.getElementById("tax_county").value;
              jQuery.get(url, function(response) {
                var combo = document.getElementById("tax_settlement");
                var current = combo.options[combo.selectedIndex];
                combo.options.length = 0;
                combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, current == '');
                combo.options[0].disabled = true;
                var catalog = JSON.parse(JSON.stringify(response));
                if(catalog != null)
                  for(var i = 0; i < catalog.length; i++)
                    combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, catalog[i].key == current);
                update_tax_postal_code();
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_tax_settlements( ...

          /**
           Update the available tax counties inside of its own combo box from the current tax country and state.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_tax_counties() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=counties" +
                        "&country=" + document.getElementById("tax_country").value +
                        "&state=" + document.getElementById("tax_state").value;
              jQuery.get(url, function(response) {
                var combo = document.getElementById("tax_county");
                var current = combo.options[combo.selectedIndex];
                combo.options.length = 0;
                combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, current == '');
                combo.options[0].disabled = true;
                var catalog = JSON.parse(JSON.stringify(response));
                if(catalog != null)
                  for(var i = 0; i < catalog.length; i++)
                    combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, catalog[i].key == current);
                update_tax_settlements();
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_tax_counties( ...

          /**
           Update the available tax states inside of its own combo box from the current tax country.
           
           @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
           @since      1.0, Initial release
           */
          function update_tax_states() {
            try {
              var url = "<?php echo get_site_url() ?>/wp-admin/admin-ajax.php" +
                        "?action=update_postal_codes" +
                        "&code=states" +
                        "&country=" + document.getElementById("tax_country").value;
              jQuery.get(url, function(response) {
                var combo = document.getElementById("tax_state");
                var current = combo.options[combo.selectedIndex];
                combo.options.length = 0;
                combo.options[0] = new Option("<?php _e('Select one', 'organizations') ?>", "", true, current == '');
                combo.options[0].disabled = true;
                var catalog = JSON.parse(JSON.stringify(response));
                if(catalog != null)
                  for(var i = 0; i < catalog.length; i++)
                    combo.options[i + 1] = new Option(catalog[i].name, catalog[i].key, false, catalog[i].key == current);
                update_tax_counties();
              }); // jQuery.get(url, function(response)
            } catch(e) { } // try { ... } catch(e) { ... }
          } // function update_tax_states( ...
          </script>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="tax_country">
                  <?php esc_html_e('Country', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="tax_country" name="tax_country" onChange="update_tax_states();">
                  <option value="" disabled<?php echo($row['tax_country'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option>
                  <?php
                $sql = 'SELECT DISTINCT country AS country 
                        FROM gt_settlements
                        ORDER BY country';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->country) ?>"<?php echo($catalog->country == $row['tax_country'] ? 'selected' : '') ?>>
                    <?php echo $catalog->country ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_state">
                  <?php esc_html_e('State, department or province', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="tax_state" name="tax_state" onChange="update_tax_counties();">
                  <option value="" disabled<?php echo($row['tax_state'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT state AS state
                        FROM gt_settlements
                        WHERE country = "' . $row['tax_country'] . '"
                        ORDER BY state';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->state) ?>"<?php echo($catalog->state == $row['tax_state'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->state) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_county">
                  <?php esc_html_e('Municipality, delegation or county', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="tax_county" name="tax_county" onChange="update_tax_settlements();">
                  <option value="" disabled<?php echo($row['tax_county'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT county AS county
                        FROM gt_settlements
                        WHERE country = "' . $row['tax_country'] . '" AND
                              state   = "' . $row['tax_state'] . '"
                       ORDER BY county';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->county) ?>"<?php echo($catalog->county == $row['tax_county'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->county) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_settlement">
                  <?php esc_html_e('Settlement', 'organizations') ?>:
                </label>
              </th>
              <td>
                <select id="tax_settlement" name="tax_settlement" onChange="update_tax_postal_code();">
                  <option value="" disabled<?php echo($row['tax_settlement'] == '' ? ' selected' : '') ?>>
                    <?php esc_html_e('Select one', 'organizations') ?>
                  </option><?php
                $sql = 'SELECT DISTINCT settlement AS settlement
                        FROM gt_settlements
                        WHERE country = "' . $row['tax_country'] . '" AND
                              state   = "' . $row['tax_state'] . '" AND
                              county  = "' . $row['tax_county'] . '"
                        ORDER BY settlement';
                $results = $wpdb->get_results($sql);
                foreach($results as $catalog) { ?>
                  <option value="<?php echo esc_attr($catalog->settlement) ?>"<?php echo($catalog->settlement == $row['tax_settlement'] ? 'selected' : '') ?>>
                    <?php echo esc_html($catalog->settlement) ?>
                  </option>
                <?php } // foreach($results as $catalog) ?>
                </select>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_postal_code">
                  <?php esc_html_e('Postal code', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_postal_code" name="tax_postal_code" type="number" min="1" max="99999" step="1" maxlength="5" aria-describedby="tax_postal_code-description" class="regular-text" value="<?php echo esc_attr($row['tax_postal_code']) ?>" onChange="update_tax_address();" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_street">
                  <?php esc_html_e('Street', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_street" name="tax_street" type="text" maxlength="100" aria-describedby="tax_street-description" class="regular-text" value="<?php echo esc_attr($row['tax_street']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_outside">
                  <?php esc_html_e('Exterior number', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_outside" name="tax_outside" type="text" maxlength="30" aria-describedby="tax_outside-description" class="regular-text" value="<?php echo esc_attr($row['tax_outside']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_inside">
                  <?php esc_html_e('Interior number', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_inside" name="tax_inside" type="text" maxlength="30" aria-describedby="tax_inside-description" class="regular-text" value="<?php echo esc_attr($row['tax_inside']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="tax_reference">
                  <?php esc_html_e('Reference', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="tax_reference" name="tax_reference" type="text" maxlength="100" aria-describedby="tax_reference-description" class="regular-text" value="<?php echo esc_attr($row['tax_reference']) ?>" />
              </td>
            </tr>
          </table>
          <h1>
            <?php esc_html_e('Phones', 'organizations') ?>
          </h1>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="phone_1">
                  <?php esc_html_e('Main', 'organizations') ?> 1:
                </label>
              </th>
              <td>
                <input id="phone_1" name="phone_1" type="tel" pattern="[0-9]*" value="<?php echo esc_attr($row['phone_1']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="phone_2">
                  <?php esc_html_e('Main', 'organizations') ?> 2:
                </label>
              </th>
              <td>
                <input id="phone_2" name="phone_2" type="tel" pattern="[0-9]*" value="<?php echo esc_attr($row['phone_2']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="phone_3">
                  <?php esc_html_e('Main', 'organizations') ?> 3:
                </label>
              </th>
              <td>
                <input id="phone_3" name="phone_3" type="tel" pattern="[0-9]*" value="<?php echo esc_attr($row['phone_3']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="mobile">
                <?php esc_html_e('Mobile', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="mobile" name="mobile" type="tel" pattern="[0-9]*" value="<?php echo esc_attr($row['mobile']) ?>" />
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="fax">
                  <?php esc_html_e('Fax', 'organizations') ?>:
                </label>
              </th>
              <td>
                <input id="fax" name="fax" type="tel" pattern="[0-9]*" value="<?php echo esc_attr($row['fax']) ?>" />
              </td>
            </tr>
          </table>
          <h1>
            <?php esc_html_e('Media', 'organizations') ?>
          </h1>
          <table class="form-table">
            <tr>
              <th scope="row">
                <label for="logo_img">
                  <?php esc_html_e('Logo, photo or avatar', 'organizations') ?>:
                </label>
              </th>
              <td>
                <?php $exists = is_array($logo_src = wp_get_attachment_image_src(intval($row['logo']))) ?>
                <input id="logo" name="logo" type="hidden" value="<?php echo esc_attr($row['logo']) ?>" />
                <input id="logo_url" name="logo_url" type="hidden" value="<?php echo esc_attr($logo_src[0]) ?>" />
                <script type="text/javascript">
                /**
                 Media Uploader handler. This instance is used to manipulate the WordPress Media Library Uplooder.
                 
                 @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
                 @since      1.0, Initial definition of the variable
                 */
                var media_uploader = null;

                /**
                 Open media uploader for a logo.
                 
                 @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
                 @since      1.0, Initial release
                 */
                function open_logo_media_uploader() {
                  media_uploader = wp.media({
                      frame:    "post", 
                      state:    "insert", 
                      multiple: false
                    }); // media_uploader = wp.media( ...
                  media_uploader.on("insert", function() {
                      var json = media_uploader.state().get("selection").first().toJSON();
                      document.getElementById("logo_div").display = "block";
                      document.getElementById("logo_img").src = json.url;
                      document.getElementById("logo_img").style.display = 'block';
                      document.getElementById("logo_url").value = json.url;
                      document.getElementById("logo").value = json.id;
                    }); // media_uploader.on("insert", function( ...
                  media_uploader.on("select", function() {
                      var json = media_uploader.state().get("selection").first().toJSON();
                      document.getElementById("logo_div").display = "block";
                      document.getElementById("logo_img").src = json.url;
                      document.getElementById("logo_img").style.display = 'block';
                      document.getElementById("logo_url").value = json.url;
                      document.getElementById("logo").value = json.id;
                    }); // media_uploader.on('select', function( ...
                  media_uploader.open();
                } // function open_logo_media_uploader( ...
                </script>
                <div id="logo_div" name="logo_div" class="custom-img-container">
                  <img id="logo_img" name="logo_img" src="<?php echo esc_attr($logo_src[0]) ?>"
                        alt="<?php esc_attr_e('Organization\'s logo', 'organizations') ?>"
                        style="max-height: 256px;<?php echo ' display: ' . (!$exists ? 'none' : 'block') ?>" width="256" height="256" class="avatar photo" />
                </div>
                <p class="hide-if-no-js">
                  <a class="upload-custom-img button button-primary" onClick="open_logo_media_uploader();">
                    <?php esc_html_e('Set image', 'organization') ?>
                  </a>
                  <?php
                  if($exists)
                    echo '<a class="delete-custom-img button button-primary" ' .
                            'href="/wp-admin/options-general.php' .
                                   '?page=organizations' .
                                   '&action=remove_logo' .
                                   '&organization=' . urlencode($_REQUEST['organization']) .
                                   '&paged=' . urlencode (intval($_REQUEST['paged'])) .
                                   '&deleted=' . intval($_REQUEST['deleted']) .
                                   '&per_page=' . intval($_REQUEST['per_page']) .
                                   '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                            esc_html__('Remove image', 'organization') .
                          '</a>';
                  ?>
                </p>
                <p class="description" id="organization-description">
                  <?php esc_html_e('Generrally is used to identify this organization inside of Galatea\'s applications. For example, in Balanced Scorecard this file is showed in the views of every Key Performance Indicator and Perspective of this organization. We recommend an image file in the format of PNG (Portable Network Graphic) with 512 pixeles of width per 512 pixels of height', 'organizations') ?>
                </p>
              </td>
            </tr>
            <tr>
              <th scope="row">
                <label for="template_img">
                  <?php esc_html_e('Invoice template', 'organizations') ?>:
                </label>
              </th>
              <td>
                <?php $exists = is_array($template_src = wp_get_attachment_image_src(intval($row['template']))) ?>
                <input id="template" name="template" type="hidden" value="<?php echo esc_attr($row['template']) ?>" />
                <input id="template_url" name="template_url" type="hidden" value="<?php echo esc_attr($template_src[0]) ?>" />
                <script type="text/javascript">
                /**
                 Open media uploader for a template.
                 
                 @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
                 @since      1.0, Initial release
                 */
                function open_template_media_uploader_image() {
                  media_uploader = wp.media({
                      frame:    "post", 
                      state:    "insert", 
                      multiple: false
                    }); // media_uploader = wp.media( ...
                  media_uploader.on("insert", function() {
                      var json = media_uploader.state().get("selection").first().toJSON();
                      document.getElementById("template_div").display = "block";
                      document.getElementById("template_img").style.display = 'block';
                      document.getElementById("template_url").value = json.url;
                      document.getElementById("template").value = json.id;
                    }); // media_uploader.on("insert", function( ...
                  media_uploader.on("select", function() {
                      var json = media_uploader.state().get("selection").first().toJSON();
                      document.getElementById("template_div").display = "block";
                      document.getElementById("template_img").style.display = 'block';
                      document.getElementById("template_url").value = json.url;
                      document.getElementById("template").value = json.id;
                    }); // media_uploader.on('select', function( ...
                  media_uploader.open();
                } // function open_template_media_uploader_image( ...
                </script>
                <div id="template_div" name="template_div" class="custom-img-container">
                  <img id="template_img" name="template_img" src="<?php echo esc_attr(plugin_dir_url(__FILE__)) ?>images/xlsx.png" alt="<?php esc_attr_e('Organization\'s invoices template', 'organizations') ?>" style="max-height: 256px;<?php echo ' display: ' . (!$exists ? 'none' : 'block') ?>" width="256" height="256" class="avatar photo" />
                </div>
                <p class="hide-if-no-js">
                  <a class="upload-custom-img button button-primary" onClick="open_template_media_uploader_image();">
                    <?php esc_html_e('Set template', 'organization') ?>
                  </a>
                  <?php
                  if($exists)
                    echo '<a class="delete-custom-img button button-primary" ' .
                            'href="/wp-admin/options-general.php' .
                                   '?page=organizations' .
                                   '&action=remove_template' .
                                   '&organization=' . urlencode($_REQUEST['organization']) .
                                   '&paged=' . urlencode (intval($_REQUEST['paged'])) .
                                   '&deleted=' . intval($_REQUEST['deleted']) .
                                   '&per_page=' . intval($_REQUEST['per_page']) .
                                   '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                           esc_html__('Remove template', 'organization') .
                         '</a>';
                  ?>
                </p>
                <p class="description" id="organization-description">
                  <?php esc_html_e('Contains an spreadsheet of Microsoft Excel XLSX 2007 with the invoices template used by this organization for issuing its own invoices. If you skip this template, the electronic invoicing services uses the default invoice template defined by your electronic invoicing services provider.', 'organizations') ?>
                </p>
              </td>
            </tr>
          </table>
          <?php submit_button() ?>
        </form>
      </div><!-- wrap -->
      <?php
      return;
    } // function edit_organization( ...
  } // if(!function_exists('edit_organization'))

  // Define the function only if this doesn't exists
  if(!function_exists('enqueue_wordpress_security_enhacements')) {
    add_action('wp_enqueue_scripts', 'enqueue_wordpress_security_enhacements');
    function enqueue_wordpress_security_enhacements() {
      // your enqueue will probably look different.
      wp_enqueue_script('wwpse_script');

      // Localize the script
      $data = array( 
          'ajax_url' => admin_url('admin-ajax.php'),
          'nonce'    => wp_create_nonce('organizations-enqueue_wwpse')
        );
      wp_localize_script('wordpress_security_enhacements_script',
                         'wordpress_security_enhacements_object',
                         $data);
    } // function enqueue_wordpress_security_enhacements()
  } // if(!function_exists('enqueue_wordpress_security_enhacements'))

  // Define the function only if this doesn't exists
  if(!function_exists('enqueue_media_uploader')) {
    add_action("admin_enqueue_scripts", "enqueue_media_uploader");
    /**
     Enqueue the Media Library Uploader libraries.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function enqueue_media_uploader() {
      wp_enqueue_media();
    } // function enqueue_media_uploader( ...
  } // if(!function_exists('enqueue_media_uploader'))

  // Define the function only if this doesn't exists
  if(!function_exists('init_organizations')) {
    add_action('admin_init', 'init_organizations');
    /**
     Init settings, variables and tables.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function init_organizations() {
      // Loads the localisation configuration files
      if(file_exists(plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . get_locale() . '.mo'))
        load_textdomain('organizations', plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . get_locale() . '.mo');
      elseif(file_exists(plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . substr(get_locale(), 0, 2) . '.mo'))
        load_textdomain('organizations', plugin_basename(dirname(__FILE__)) . '/languages/organizations-' . substr(get_locale(), 0, 2) . '.mo');
      load_plugin_textdomain('organizations',  false, plugin_basename(dirname(__FILE__)) . '/languages/');

      // Get de current Database WordPress connection
      global $wpdb;

      /**
       Table of organizations. Contains the system organizations information. In this table is located the organizations registry under which and/or through which your organization operates itself.
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        organization         | varchar |     255  | Organization email
        name                 | varchar |     100  | Organization name
        description          | varchar |          | Organization description
        main_street          | varchar |     100  | Main residence's Street
        main_outside         | varchar |      30  | Main residence's Exterior number
        main_inside          | varchar |      30  | Main residence's Interior number
        main_reference       | varchar |     100  | Main residence's Reference
        main_settlement      | varchar |     100  | Main residence's Settlement
        main_postal_code     | varchar |      10  | Main residence's Postal code
        main_county          | varchar |     100  | Main residence's Municipality, delegation or county
        main_state           | varchar |     100  | Main residence's State, department or province
        main_country         | varchar |     100  | Main residence's Country
        tax_name             | varchar |     255  | Federal Taspayer Name
        tax_id               | varchar |      13  | Federal Taspayer Registration Number
        tax_regime           | varchar |      10  | Federal Tax Regime
        serial               | varchar |      25  | Current serial number of invoices
        currency             | varchar |       3  | Currency code registered in gt_currency_codes
        way_to_pay           | varchar |       3  | Way to pay code registered in gt_ways_to_pay
        payment_method       | varchar |       3  | Payment metthod code registered in gt_payment_methods
        use_of_voucher       | varchar |       3  | Currency code registered in gt_uses_of_vouchers
        certificate_number   | varchar |     255  | Certificate number of invoices
        einvoicing_url       | varchar |     255  | URL of WebService of facturehoy.com
        einvoicing_username  | varchar |     255  | Username of WebService of facturehoy.com
        einvoicing_password  | varchar |     255  | Password of WebService of facturehoy.com
        einvoicing_service   | varchar |      25  | Number of service of WebService of facturehoy.com
        tax_street           | varchar |     100  | Tax residence's Street
        tax_outside          | varchar |      30  | Tax residence's Exterior number
        tax_inside           | varchar |      30  | Tax residence's Interior number
        tax_reference        | varchar |     100  | Tax residence's Reference
        tax_settlement       | varchar |     100  | Tax residence's Settlement
        tax_postal_code      | varchar |      10  | Tax residence's Postal code
        tax_county           | varchar |     100  | Tax residence's Municipality, delegation or county
        tax_state            | varchar |     100  | Tax residence's State, department or province
        tax_country          | varchar |     100  | Tax residence's Country
        phone_1              | varchar |      25  | 1st. Main Phone
        phone_2              | varchar |      25  | 2nd. Main Phone
        phone_3              | varchar |      25  | 3rd. Main Phone
        mobile               | varchar |      25  | Mobile Phone
        fax                  | varchar |      25  | Fax Phone
        logo                 | int     |      10  | Organization logo
        template             | int     |      10  | Invoices template
        deleted              | tinyint |       3  | This record was status?
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       Primary table keys: organization
       
       @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since      1.0, Initial release
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_organizations (
                organization         varchar(255)   NOT NULL,
                name                 varchar(100)   NOT NULL,
                description          text           NOT NULL,
                main_street          varchar(100)   NOT NULL,
                main_outside         varchar(30)    NOT NULL,
                main_inside          varchar(30)    NOT NULL,
                main_reference       varchar(100)   NOT NULL,
                main_settlement      varchar(100)   NOT NULL,
                main_postal_code     varchar(10)    NOT NULL,
                main_county          varchar(100)   NOT NULL,
                main_state           varchar(100)   NOT NULL,
                main_country         varchar(100)   NOT NULL,
                tax_name             varchar(255)   NOT NULL,
                tax_id               varchar(13)    NOT NULL,
                tax_regime           varchar(10)    NOT NULL,
                serial               varchar(25)    NOT NULL,
                currency             varchar(3)     NOT NULL,
                way_to_pay           varchar(10)    NOT NULL,
                payment_method       varchar(3)     NOT NULL,
                use_of_voucher       varchar(8)     NOT NULL,
                certificate_number   varchar(255)   NOT NULL,
                einvoicing_url       varchar(255)   NOT NULL,
                einvoicing_username  varchar(255)   NOT NULL,
                einvoicing_password  varchar(255)   NOT NULL,
                einvoicing_service   varchar(25)    NOT NULL,
                tax_street           varchar(100)   NOT NULL,
                tax_outside          varchar(30)    NOT NULL,
                tax_inside           varchar(30)    NOT NULL,
                tax_reference        varchar(100)   NOT NULL,
                tax_settlement       varchar(100)   NOT NULL,
                tax_postal_code      varchar(10)    NOT NULL,
                tax_county           varchar(100)   NOT NULL,
                tax_state            varchar(100)   NOT NULL,
                tax_country          varchar(100)   NOT NULL,
                phone_1              varchar(25)    NOT NULL,
                phone_2              varchar(25)    NOT NULL,
                phone_3              varchar(25)    NOT NULL,
                mobile               varchar(25)    NOT NULL,
                fax                  varchar(25)    NOT NULL,
                logo                 int(10)        NOT NULL,
                template             int(10)        NOT NULL,
                deleted              tinyint(3)     NOT NULL,
                editor               varchar(255)   NOT NULL,
                edition              varchar(14)    NOT NULL,
              PRIMARY KEY(organization))
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);

      /**
       Table of currency codes. Contains the currency codes of 3 alphanumeric chars specified into the ISO 4217.
       
              Field        |  Type   |  Length  | Description
       --------------------+---------+----------+---------------------------------------------------------------
        code               | varchar |       3  | Currency code based in 3 alphanumeric chars
        description        | varchar |     255  | Currency description
        decimals           | tinyint |       3  | Number of decimal digits
        variation          | double  |   18, 6  | Variation percentaje
        deleted            | tinyint |       3  | This record was status?
        editor             | varchar |     255  | Who did edit this registry? (Email)
        edition            | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       Primary table keys: code
       
       @author    Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since     1.0, Initial definition of the table
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_currency_codes (
                code       varchar(3)     NOT NULL,
                name       varchar(255)   NOT NULL,
                decimals   tinyint(3)     NOT NULL,
                variation  double(18, 6)  NOT NULL,
                deleted    tinyint(3)     NOT NULL,
                editor     varchar(255)   NOT NULL,
                edition    varchar(14)    NOT NULL,
              PRIMARY KEY(code))
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);
      $sql = 'SELECT code
              FROM gt_currency_codes';
      $row = $wpdb->get_row($sql);
      if($wpdb->num_rows == 0 || is_null($row)) {
        $sql = 'INSERT INTO gt_currency_codes VALUES
                  ("MXN", "Pesos mexicanos",    2, 0.50, 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("USD", "Dólares americanos", 2, 0.50, 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("EUR", "Euros",              2, 0.50, 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
        $wpdb->query($sql);
      } // if($wpdb->num_rows == 0 || is_null($row))

      /**
       Table of ways to pay. This table contains the available and allowed ways to pay for registered invoices.
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        code                 | varchar |      10  | Code
        name                 | varchar |     255  | Name
        deleted              | tinyint |       3  | This record was status?
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       Primary table keys: code
       
       @author    Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since     1.0, Initial definition of the table
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_ways_to_pay (
                code         varchar(10)   NOT NULL,
                name         varchar(255)  NOT NULL,
                deleted      tinyint(3)    NOT NULL,
                editor       varchar(255)  NOT NULL,
                edition      varchar(14)   NOT NULL,
              PRIMARY KEY(code)) 
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);
      $sql = 'SELECT code
              FROM gt_ways_to_pay';
      $row = $wpdb->get_row($sql);
      if($wpdb->num_rows == 0 || is_null($row)) {
        $sql = 'INSERT INTO gt_ways_to_pay VALUES
                  ("01", "Efectivo",                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("02", "Cheque nominativo",                   0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("03", "Transferencia electrónica de fondos", 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("04", "Tarjeta de crédito",                  0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("05", "Monedero electrónico",                0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("06", "Dinero electrónico",                  0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("08", "Vales de despensa",                   0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("12", "Dación en pago",                      0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("13", "Pago por subrogación",                0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("14", "Pago por consignación",               0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("15", "Condonación",                         0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("17", "Compensación",                        0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("23", "Novación",                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("24", "Confusión",                           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("25", "Remisión de deuda",                   0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("26", "Prescripción o caducidad",            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("27", "A satisfacción del acreedor",         0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("28", "Tarjeta de débito",                   0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("29", "Tarjeta de servicios",                0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("30", "Aplicación de anticipos",             0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("31", "Intermediario pagos",                 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
        $wpdb->query($sql);
      } // if($wpdb->num_rows == 0 || is_null($row))

      /**
       Table of payment methods. This table contains the available and allowed payment methods for registered invoices.
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        code                 | varchar |      10  | Code
        name                 | varchar |     255  | Name
        deleted              | tinyint |       3  | This record was status?
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       Primary table keys: code
       
       @author    Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since     1.0, Initial definition of the table
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_payment_methods (
                code         varchar(3)    NOT NULL,
                name         varchar(255)  NOT NULL,
                deleted      tinyint(3)    NOT NULL,
                editor       varchar(255)  NOT NULL,
                edition      varchar(14)   NOT NULL,
              PRIMARY KEY(code)) 
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);
      $sql = 'SELECT code
              FROM gt_payment_methods';
      $row = $wpdb->get_row($sql);
      if($wpdb->num_rows == 0 || is_null($row)) {
        $sql = 'INSERT INTO gt_payment_methods VALUES
                  ("PUE", "Pago en una sola exhibición",      0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("PPD", "Pago en parcialidades o diferido", 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
        $wpdb->query($sql);
      } // if($wpdb->num_rows == 0 || is_null($row))

      /**
       Table of tax regimes. This table contains the regime tax codes used by the organizations for registered invoices.
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        code                 | varchar |       3  | Code
        description          | varchar |     255  | Description
        deleted              | tinyint |       3  | This record was status?
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       Primary table keys: code
       
       @author    Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since     1.0, Initial definition of the table
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_tax_regimes (
                code         varchar(10)   NOT NULL,
                name         varchar(255)  NOT NULL,
                deleted      tinyint(3)    NOT NULL,
                editor       varchar(255)  NOT NULL,
                edition      varchar(14)   NOT NULL,
              PRIMARY KEY(code))
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);
      $sql = 'SELECT code
              FROM gt_tax_regimes';
      $row = $wpdb->get_row($sql);
      if($wpdb->num_rows == 0 || is_null($row)) {
        $sql = 'INSERT INTO gt_tax_regimes VALUES
                  ("601", "General de Ley Personas Morales",                                          0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("603", "Personas Morales con Fines No Lucrativos",                                 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("605", "Sueldos y Salarios e Ingresos Asimilados a Salarios",                      0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("606", "Arrendamiento",                                                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("608", "Demás ingresos",                                                           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("609", "Consolidación",                                                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("610", "Residentes en el Extranjero sin Establecimiento Permanente en México",     0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("611", "Ingresos por Dividendos (socios y accionistas)",                           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("612", "Personas Físicas con Actividades Empresariales y Profesionales",           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("614", "Ingresos por intereses",                                                   0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("616", "Sin obligaciones fiscales",                                                0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("620", "Sociedades Cooperativas de Producción que optan por diferir sus ingresos", 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("621", "Incorporación Fiscal",                                                     0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("622", "Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras",                 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("623", "Opcional para Grupos de Sociedades",                                       0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("624", "Coordinados",                                                              0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("628", "Hidrocarburos",                                                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("607", "Régimen de Enajenación o Adquisición de Bienes",                           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("629", "De los Regímenes Fiscales Preferentes y de las Empresas Multinacionales",  0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("630", "Enajenación de acciones en bolsa de valores",                              0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("615", "Régimen de los ingresos por obtención de premios",                         0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
        $wpdb->query($sql);
      } // if($wpdb->num_rows == 0 || is_null($row))

      /**
       Table of uses of vouchers. It contains the available and allowed concept units for registered invoices.
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        code                 | varchar |       8  | Code
        description          | varchar |     255  | Description
        deleted              | tinyint |       3  | This record was status?
        deleted              | tinyint |       3  | This record was status?
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       Primary table keys: code
       
       @author    Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since     1.0, Initial definition of the table
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_uses_of_vouchers (
                code         varchar(8)    NOT NULL,
                name         varchar(255)  NOT NULL,
                deleted      tinyint(3)    NOT NULL,
                editor       varchar(255)  NOT NULL,
                edition      varchar(14)   NOT NULL,
              PRIMARY KEY(code))
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);
      $sql = 'SELECT code
              FROM gt_uses_of_vouchers';
      $row = $wpdb->get_row($sql);
      if($wpdb->num_rows == 0 || is_null($row)) {
        $sql = 'INSERT INTO gt_uses_of_vouchers VALUES
                  ("G01", "Adquisición de mercancias",                                                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("G02", "Devoluciones, descuentos o bonificaciones",                                            0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("G03", "Gastos en general",                                                                    0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I01", "Construcciones",                                                                       0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I02", "Mobilario y equipo de oficina por inversiones",                                        0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I03", "Equipo de transporte",                                                                 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I04", "Equipo de computo y accesorios",                                                       0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I05", "Dados, troqueles, moldes, matrices y herramental",                                     0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I06", "Comunicaciones telefónicas",                                                           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I07", "Comunicaciones satelitales",                                                           0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("I08", "Otra maquinaria y equipo",                                                             0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D01", "Honorarios médicos, dentales y gastos hospitalarios.",                                 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D02", "Gastos médicos por incapacidad o discapacidad",                                        0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D03", "Gastos funerales",                                                                     0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D04", "Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación)",   0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D06", "Aportaciones voluntarias al SAR.",                                                     0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D07", "Primas por seguros de gastos médicos.",                                                0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D08", "Gastos de transportación escolar obligatoria.",                                        0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D09", "Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones", 0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")),
                  ("D10", "Pagos por servicios educativos (colegiaturas)",                                        0, "' . $current_user . '", DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
        $wpdb->query($sql);
      } // if($wpdb->num_rows == 0 || is_null($row))

      /**
       Table of settlements. Contains all postal codes of all allowed addresses by Galateas system. Inside of this catalog are registered the official postal codes and settlements emitted by postal authoritthies in every different country.
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        id                   | int     |      10  | Settlement's ID
        postal_code          | varchar |      10  | Settlement's Postal code
        settlement           | varchar |      10  | Settlement's Name
        county               | varchar |     100  | Settlement's Municipality, delegation or county
        state                | varchar |     100  | Settlement's State, department or province
        country              | varchar |     100  | Settlement's Country
        deleted              | tinyint |       3  | This record was status?
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
       
       @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since      1.0, Initial release
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_settlements (
                id            int(10) UNSIGNED  NOT NULL,
                postal_code   varchar(10)       NOT NULL,
                settlement    varchar(100)      NOT NULL,
                county        varchar(100)      NOT NULL,
                state         varchar(100)      NOT NULL,
                country       varchar(100)      NOT NULL,
                deleted       tinyint(3)        NOT NULL,
                editor        varchar(100)      NOT NULL,
                edition       varchar(14)       NOT NULL,
                PRIMARY KEY(id))
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);

      /**
       Table of user logs. Contains the Log messages of Galatea's system where is located the activities registry of all users. 
       
               Field         |  Type   |  Length  | Description
       ----------------------+---------+----------+---------------------------------------------------------------
        editor               | varchar |     255  | Who did edit this registry? (Email)
        edition              | varchar |      14  | When was edited this registry? (as AAAAMMDDhhmmss)
        event                | text    |          | Specifications of this event
       
       @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
       @since      1.0, Initial release
       */
      $sql = 'CREATE TABLE IF NOT EXISTS gt_logs (
                editor   varchar(255)  NOT NULL,
                edition  varchar(14)   NOT NULL,
                event    text          NOT NULL)
              ENGINE=MyISAM DEFAULT CHARSET=utf8;';
      $wpdb->query($sql);

      return;
    } // function init_organizations( ...
  } // if(!function_exists('init_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('list_organizations')) {
    /**
     Print the list of records.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function list_organizations() {
      // Get de current Database WordPress connection and nonce
      global $wpdb;
      
      // Loads the plugin utilities library
      include_once(ABSPATH . '/wp-admin/includes/plugin.php');

      // If PhpOffice/PhpSpreadsheet isn't active
      if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . printf(__('%s recommends the plugin of %s', 'organizations'),
                              esc_html__('Organizations of Galatea', 'organizations'),
                              esc_html__('PhpOffice/PhpSpreadsheet of Galatea', 'organizations')) . '</p>' .
             '</div>';
      } // if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php'))

      // If PhpOffice/PhpSpreadsheet isn't active
      if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . printf(__('%s recommends the plugin of %s', 'organizations'),
                              esc_html__('Organizations of Galatea', 'organizations'),
                              esc_html__('PhpOffice/PhpSpreadsheet of Galatea', 'organizations')) . '</p>' .
             '</div>';
      } // if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php'))

      // Get the current active and deleted number of records
      $sql = 'SELECT COUNT(*)
              FROM gt_organizations
              WHERE deleted = 0';
      $active_items = $wpdb->get_var($sql);
      $sql = 'SELECT COUNT(*)
              FROM gt_organizations
              WHERE deleted = 1';
      $deleted_items = $wpdb->get_var($sql);
      $active_items  = ($active_items > 0 ? intval($active_items) : 0);
      $deleted_items = ($deleted_items > 0 ? intval($deleted_items) : 0);
      $all = ($_REQUEST['deleted'] == 0 ? $active_items : $deleted_items);
      $total_pages = round($all / (intval($_REQUEST['per_page']) > 20 ?
                                   intval($_REQUEST['per_page']) : 20), 0, PHP_ROUND_HALF_UP);
      if($total_pages < 1)
        $total_pages = 1;
      if($_REQUEST['paged'] < 1)
        $_REQUEST['paged'] = 1;
      if($_REQUEST['deleted'] != 0 && $_REQUEST['deleted'] != 1)
        $_REQUEST['deleted'] = 0;
      if($_REQUEST['per_page'] <= 20)
        $_REQUEST['per_page'] = 20;
      ?>
      <div class="wrap">
        <h1 class="wp-heading-inline">
          <?php esc_html_e('Organizations', 'organizations') ?>
        </h1>
        <?php
        if($_REQUEST["deleted"] == 0) {
          echo '<a class="page-title-action" ' .
                  'href="/wp-admin/options-general.php' .
                         '?page=organizations' .
                         '&action=edit' .
                         '&organization=' .
                         '&paged=' . urlencode (intval($_REQUEST['paged'])) .
                         '&deleted=0' .
                         '&per_page=' . intval($_REQUEST['per_page']) .
                         '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                 esc_html__('Add new', 'organizations') .
               '</a>';
          if(is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php')) {
            if($active_items > 0 && is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php'))
              echo '<a class="page-title-action" ' .
                      'href="/wp-admin/admin-ajax.php' .
                             '?action=download_organizations' .
                             '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                     esc_html__('Download', 'organizations') .
                   '</a>';
            echo '<form action="/wp-admin/options-general.php" method="post" enctype="multipart/form-data" id="upload-action-form" ' .
                       'style="display: inline;">' .
                   '<input type="hidden" name="page" value="organizations" />' .
                   '<input type="hidden" name="action" value="upload" />' .
                   '<input type="hidden" name="paged" value="' . esc_attr($_REQUEST['paged']) . '" />' .
                   '<input type="hidden" name="deleted" value="' . esc_attr($_REQUEST['deleted']) . '" />' .
                   '<input type="hidden" name="per_page" value="' . esc_attr($_REQUEST['per_page']) . '" />' .
                   '<input type="hidden" name="_wpnonce" value="' . wp_create_nonce('organizations-nonce') . '" />' .
                   wp_referer_field(false) .
                   '<a id="upload_action" class="page-title-action" ' .
                      'onClick="document.getElementById(\'uploaded_file\').click();">' .
                     esc_html__('Upload', 'organizations') .
                   '</a>' .
                   '<input name="uploaded_file" id="uploaded_file" type="file" ' .
                          'style="opacity: 0; position: absolute; z-index: -1;" ' .
                          'onChange="document.getElementById(\'upload-action-form\').submit();" ' .
                          'accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" />' .
                 '</form>';
          };
        } // if($_REQUEST["deleted"] == 0)
        ?>
        <hr class="wp-header-end">
        <h2 class="screen-reader-text">
          <?php esc_html_e('Filter', 'organizations') ?>
        </h2>
        <ul class="subsubsub">
          <li class="all">
            <?php
            echo '<a' . ($_REQUEST['deleted'] == 0 ? ' class="current" aria-current="page"' : '') .
                    'href="/wp-admin/options-general.php' .
                           '?page=organization' .
                           '&action=list' .
                           '&paged=' . urlencode (intval($_REQUEST['paged'])) .
                           '&deleted=0' .
                           '&per_page=' . intval($_REQUEST['per_page']) .
                           '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                   esc_html__('All', 'organizations') . ' (' . number_format($active_items, 0) . ')' .
                 '</a>';
            ?>
          </li>
          <li class="all">
            &nbsp;|&nbsp;
            <?php
            echo '<a' . ($_REQUEST['deleted'] == 1 ? ' class="current" aria-current="page"' : '') .
                    'href="/wp-admin/options-general.php' .
                           '?page=organizations' .
                           '&action=list' .
                           '&paged=' . urlencode (intval($_REQUEST['paged'])) .
                           '&deleted=1' .
                           '&per_page=' . intval($_REQUEST['per_page']) .
                           '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                   esc_html__('Trash', 'organizations') . ' (' . number_format($deleted_items, 0) . ')' .
                 '</a>';
            ?>
          </li>
        </ul>
        <form action="/wp-admin/options-general.php" method="post" enctype="multipart/form-data" id="bulk-action-form">
          <input type="hidden" name="page" value="organizations" />
          <input type="hidden" name="action" value="list" />
          <input type="hidden" name="paged" value="<?php echo esc_attr($_REQUEST['paged']) ?>" />
          <input type="hidden" name="deleted" value="<?php echo esc_attr($_REQUEST['deleted']) ?>" />
          <input type="hidden" name="per_page" value="<?php echo esc_attr($_REQUEST['per_page']) ?>" />
          <?php wp_nonce_field('organizations-nonce') ?>
          <div class="tablenav top">
            <div class="alignleft actions bulkactions">
              <label for="bulk-action-selector-top" class="screen-reader-text">
                <?php esc_html_e('Select a bulk action' , 'organizations') ?>
              </label>
              <select name="action_top" id="bulk-action-selector-top">
                <option value="-1" disabled selected>
                  <?php esc_html_e('Bulk actions', 'organizations') ?>
                </option>
                <?php if($_REQUEST['deleted'] == 0) { ?>
                  <option value="delete_selected">
                    <?php esc_html_e('Delete', 'organizations') ?>
                  </option>
                <?php } else { ?>
                  <option value="restore_selected">
                    <?php esc_html_e('Restore', 'organizations') ?>
                  </option>
                <?php } // if($_REQUEST['deleted'] == 0) { ?>
              </select>
              <input type="submit" id="doaction" class="button action"
                     value="<?php _e('Apply', 'organizations') ?>"
                   onClick="if(document.getElementById('action_top').selectedIndex != 0)
                              document.getElementById('bulk-action-form').submit();
                            return false;" />
            </div><!-- bulkactions -->
            <div class="alignleft actions">
              <?php
              if($deleted_items > 1 && $_REQUEST["deleted"] == 1)
                echo '<a class="button action" ' .
                        'href="/wp-admin/options-general.php' .
                               '?page=organizations' .
                               '&action=empty' .
                               '&paged=1' .
                               '&deleted=' . intval($_REQUEST['deleted']) .
                               '&per_page=' . intval($_REQUEST['per_page']) .
                               '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                       esc_html__('Empty trash', 'organizations') .
                     '</a>';
              ?>
            </div><!-- alignleft actions -->
            <div class="tablenav-pages <?php echo ($total_pages == 1 ? 'one-page' : '') ?>">
              <span class="displaying-num">
                (<?php echo number_format($_REQUEST['deleted'] == 0 ? $active_items : $deleted_items, 0) ?>)&nbsp;<?php esc_html_e('elements', 'organizations') ?>
              </span>
              <span class="pagination-links">
                <?php
                if($_REQUEST['paged'] > 1)
                  echo '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=1' .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('First page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>
                <?php
                if($_REQUEST['paged'] > 1)
                  echo '</a>';
                       '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=' . urlencode(intval($_REQUEST['paged']) - 1) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('Previous page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>
                <?php
                if($_REQUEST['paged'] > 1)
                  echo "</a>";
                ?>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">
                    <?php esc_html_e('Current page', 'organizations') ?>
                  </label>
                  <input class="current-page" id="current-page-selector" type="text"
                         name="paged" value="<?php echo esc_attr($_REQUEST['paged']) ?>" size="1"
                         aria-describedby="table-paging" />
                  <span class="tablenav-paging-text">
                    &nbsp;
                    <?php esc_html_e('of', 'organizations') ?>
                    &nbsp;
                    <span class="total-pages">
                      <?php echo number_format($total_pages, 0) ?>
                    </span><!-- total-pages -->
                  </span><!-- tablenav-paging-text -->
                </span><!-- paging-input -->
                <?php
                if($_REQUEST['paged'] < $total_pages)
                  echo '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=' . urlencode($_REQUEST['paged'] + 1) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('Next page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span>
                <?php
                if($_REQUEST['paged'] < $total_pages)
                  echo '</a>' .
                       '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=' . urlencode($total_pages) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('Last page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span>
                <?php
                if($_REQUEST['paged'] < $total_pages)
                  echo '</a>';
                ?>                
              </span><!-- pagination-links -->
            </div><!-- tablenav-pages -->
            <br class="clear" />
          </div><!-- tablenav top -->
          <h2 class="screen-reader-text">
            <?php esc_html_e('Organizations', 'organizations') ?>
          </h2>
          <table class="wp-list-table widefat plugins">
            <thead>
              <tr>
                <td  class="manage-column column-cb check-column">
                  <label class="screen-reader-text" for="main_checkbox">
                    <?php esc_html_e('Select all', 'organizations') ?>
                  </label>
                  <input id="main_checkbox" name="main_checkbox" type="checkbox" />
                </td>
                <th scope="col" class="manage-column column-name column-primary">
                  <?php esc_html_e('Email', 'organizations') ?>
                </th>
                <th scope="col" class="manage-column column-description">
                  <?php esc_html_e('Description', 'organizations') ?>
                </th>
              </tr>
            </thead>
            <tbody id="the-list">
            </tbody><?php
      if(($_REQUEST['deleted'] == 0 && $active_items < 1) || ($_REQUEST['deleted'] == 1 && $deleted_items < 1)) {
              ?><tr class="inactive">
                  <td colspan="3" style="text-align: center; vertical-align: middle;">
                    <?php esc_html_e('There is no records', 'organizations') ?>
                  </td>
                </tr><?php
      } else {
        $sql = 'SELECT organization,
                       name,
                       description,
                       deleted,
                       editor,
                       edition
                FROM gt_organizations
                WHERE deleted = ' . ($_REQUEST['deleted'] != 1 ? 0 : 1) . '
                ORDER BY organization
                LIMIT ' . (($_REQUEST['paged'] - 1) * $_REQUEST['per_page']) . ',
                      ' . $_REQUEST['per_page'];
        $results = $wpdb->get_results($sql);
        $counter = 0;
        foreach($results as $row) {
          if ($row->deleted == 1) { ?>
            <tr class="inactive">
          <?php } else { ?>
            <tr class="active">
          <?php } // if ($row->deleted == 1) ?>
          <th scope="row" class="check-column">
            <label class="screen-reader-text" for="checkbox_<?php echo $counter ?>">
              <?php esc_html_e('Choose', 'organizations') ?>
              &nbsp;
              <?php echo esc_html($row->organization) ?>
            </label>
            <input id="organization_<?php echo $counter ?>"  name="organizations[]" type="checkbox"
                   value="<?php echo esc_attr($row->organization) ?>" />
          </th>
          <td class="plugin-title column-primary">
            <strong>
              <?php
              if($row->deleted == 0)
                echo '<a href="/wp-admin/options-general.php' .
                               '?page=organizations' .
                               '&action=edit' .
                               '&organization=' . urlencode($row->organization) .
                               '&paged=' . urlencode($_REQUEST['paged']) .
                               '&deleted=' . intval($_REQUEST['deleted']) .
                               '&per_page=' . intval($_REQUEST['per_page']) .
                               '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
              echo esc_html($row->organization);
              if($row->deleted == 0)
                echo '</a>';
              ?>
            </strong>
            <div class="row-actions visible">
              <?php
              if($row->deleted == 1)
                echo '<span class="activate">' .
                       '<a href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=restore' .
                                 '&organization=' . urlencode($row->organization) .
                                 '&paged=' . urlencode($_REQUEST['paged']) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '" ' .
                          'aria-label="' . esc_attr__('Restore' , 'organizations') . ' ' . $row->organization . '">' .
                         esc_html_e('Restore', 'organizations') .
                       '</a>' .
                     '</span>';
              else
                echo '<span class="deactivate">' .
                       '<a href="/wp-admin/options-general.php?page=organizations' .
                                 '&action=delete' .
                                 '&organization=' . urlencode($row->organization) .
                                 '&paged=' . urlencode($_REQUEST['paged']) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '" ' .
                          'aria-label="' . esc_attr__('Delete' , 'organizations') . ' ' . $row->organization . '">' .
                         esc_html_e('Delete', 'organizations') .
                       '</a>' .
                     '</span>';
              ?>
            </div><!-- row-actions -->
          </td>
          <td class="column-description desc">
            <?php
            if($row->name != '') {
              echo '<div class="plugin-description">' .
                     '<p>' .
                       '<strong>';
              if($row->deleted == 0)
                echo     '<a href="/wp-admin/options-general.php?page=organizations' .
                                  '&action=edit' .
                                  '&organization=' . urlencode($row->organization) .
                                  '&paged=' . urlencode($_REQUEST['paged']) .
                                  '&deleted=' . intval($_REQUEST['deleted']) .
                                  '&per_page=' . intval($_REQUEST['per_page']) .
                                  '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
              echo esc_html($row->name);
              if($row->deleted == 0)
                echo     '</a>';
              echo     '</strong>' .
                     '</p>' .
                   '</div><!-- name -->';
            } // if($row->name != '')
            if($row->description != '')
              echo '<div class="plugin-description">' .
                     '<p>' .
                       '<i>' .
                         esc_html($row->description) .
                       '</i>' .
                     '</p>' .
                   '</div><!-- description -->';
            echo '<div class="active second plugin-version-author-uri">' .
                   '<p>';
            printf(__('Last edition on %s at %s by <a href="mailto:%s">%s</a>', 'organizations'),
                   date_i18n(get_option('date_format'),
                             strtotime(substr($row->edition, 4, 2) . '/' .
                                       substr($row->edition, 6, 2) . '-' .
                                       substr($row->edition, 0, 4))),
                   date_i18n(get_option('time_format'),
                             strtotime(substr($row->edition, 8, 2) . ':' .
                                       substr($row->edition, 10, 2) . ':' .
                                       substr($row->edition, 12, 2))),
                   urlencode($row->editor),
                   esc_html($row->editor));
            echo   '</p>' .
                 '</div><!-- log_record_information -->';
            ?>
          </td>
        </tr><?php
          $counter++;
        } // foreach($results as $row) 
      } // if(($_REQUEST['deleted'] == 0 && $active_items < 1) || ($_REQUEST['deleted'] == 1 && $deleted_items < 1))
          ?><tfoot>
              <tr>
                <td  class="manage-column column-cb check-column">
                  <label class="screen-reader-text" for="main_checkbox">
                    <?php esc_html_e('Select all', 'organizations') ?>
                  </label>
                  <input id="main_checkbox" name="main_checkbox" type="checkbox" />
                </td>
                <th scope="col" class="manage-column column-name column-primary">
                  <?php esc_html_e('Email', 'organizations') ?>
                </th>
                <th scope="col" class="manage-column column-description">
                  <?php esc_html_e('Description', 'organizations') ?>
                </th>
              </tr>
            </tfoot>
          </table>
          <div class="tablenav bottom">
            <div class="alignleft actions bulkactions">
              <label for="bulk-action-selector-bottom" class="screen-reader-text">
                <?php esc_html_e('Select a bulk action' , 'organizations') ?>
              </label>
              <select name="action_bottom" id="bulk-action-selector-bottom">
                <option value="-1" disabled selected>
                  <?php esc_html_e('Bulk actions', 'organizations') ?>
                </option>
                <?php if($_REQUEST['deleted'] == 0) { ?>
                  <option value="delete_selected">
                    <?php esc_html_e('Delete', 'organizations') ?>
                  </option>
                <?php } else { ?>
                  <option value="restore_selected">
                    <?php esc_html_e('Restore', 'organizations') ?>
                  </option>
                <?php } // if($_REQUEST['deleted'] == 0) { ?>
              </select>
              <input type="submit" id="doaction" class="button action"
                     value="<?php _e('Apply', 'organizations') ?>"
                   onClick="if(document.getElementById('action_bottom').selectedIndex != 0)
                              document.getElementById('bulk-action-form').submit();
                            return false;" />
            </div><!-- bulkactions -->
            <div class="alignleft actions">
              <?php
              if($deleted_items > 1 && $_REQUEST["deleted"] == 1)
                echo '<a class="button action" ' .
                        'href="/wp-admin/options-general.php' .
                              '?page=organizations' .
                              '&action=empty' .
                              '&paged=1' .
                              '&deleted=' . intval($_REQUEST['deleted']) .
                              '&per_page=' . intval($_REQUEST['per_page']) .
                              '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">' .
                       esc_html__('Empty trash', 'organizations') .
                     '</a>';
              ?>
            </div><!-- alignleft actions -->
            <div class="tablenav-pages <?php echo ($total_pages == 1 ? 'one-page' : '') ?>">
              <span class="displaying-num">
                (<?php echo number_format($_REQUEST['deleted'] == 0 ? $active_items : $deleted_items, 0) ?>)
                &nbsp;
                <?php esc_html_e('elements', 'organizations') ?>
              </span>
              <span class="pagination-links">
                <?php
                if($_REQUEST['paged'] > 1)
                  echo '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=1' .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('First page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&laquo;</span>
                <?php
                if($_REQUEST['paged'] > 1)
                  echo '</a>' .
                       '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=' . (intval($_REQUEST['paged']) - 1) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('Previous page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&lsaquo;</span>
                <?php
                if($_REQUEST['paged'] > 1)
                  echo '</a>';
                ?>
                <span class="paging-input">
                  <label for="current-page-selector" class="screen-reader-text">
                    <?php esc_html_e('Current page', 'organizations') ?>
                  </label>
                  <input class="current-page" id="current-page-selector" type="text"
                         name="paged" value="<?php echo esc_attr($_REQUEST['paged']) ?>" size="1"
                         aria-describedby="table-paging" />
                  <span class="tablenav-paging-text">
                    &nbsp;
                    <?php esc_html_e('of', 'organizations') ?>
                    &nbsp;
                    <span class="total-pages">
                      <?php echo number_format($total_pages, 0) ?>
                    </span><!-- total-pages -->
                  </span><!-- tablenav-paging-text -->
                </span><!-- paging-input -->
                <?php
                if($_REQUEST['paged'] < $total_pages)
                  echo '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=' . urlencode($_REQUEST['paged'] + 1) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('Next page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&rsaquo;</span>
                <?php
                if($_REQUEST['paged'] < $total_pages)
                  echo '</a>' .
                       '<a class="next-page" ' .
                          'href="/wp-admin/options-general.php' .
                                 '?page=organizations' .
                                 '&action=list' .
                                 '&paged=' . urlencode($total_pages) .
                                 '&deleted=' . intval($_REQUEST['deleted']) .
                                 '&per_page=' . intval($_REQUEST['per_page']) .
                                 '&_wpnonce=' . wp_create_nonce('organizations-nonce') . '">';
                ?>
                <span class="screen-reader-text">
                  <?php esc_html_e('Last page', 'organizations') ?>
                </span>
                <span class="tablenav-pages-navspan" aria-hidden="true">&raquo;</span>
                <?php
                if($_REQUEST['paged'] < $total_pages)
                  echo '</a>';
                ?>                
              </span><!-- pagination-links -->
            </div><!-- tablenav-pages -->
            <br class="clear" />
          </div><!-- tablenav bottom -->
        </form>
        <span class="spinner"></span>
      </div><!-- wrap --><?php
      return;
    } // function list_organizations( ...
  } // if(!function_exists('list_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('help_tab_organizations')) {
    /**
     Print the Preview tab.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function help_tab_organizations() {
      echo str_replace('%s', plugin_dir_url(__FILE__), __(
        '<img src="%simages/manager.jpg" alt="Galatea\'s project" style="width: 100%; height: auto;" />
<h2>Organizations of Galatea</h2>
<p>
<img align="left" src="%simages/organizations.png" alt="Organizations of Galatea" width="256" height="256" />
Organizations of Galatea is a plugin registers a page through which the registry of all organizations under which or through which the organization operates and is managed. These organizations are used, either to show the results of each company in <a href="https://github.com/mrajuarez/bsc-wordpress-1.0">Balanced Scorecard</a>, allocate budget items and structure their own departments in the module <a href="https://github.com/mrajuarez/budget_control-wordpress-1.0">Budget Control</a>, or simply to register the payments charged to credit and debit cards with <a href="https://github.com/mrajuarez/openpay.mx-wordpress-1.0">OpenPay.mx</a> and <a href="https://github.com/mrajuarez/paypal.com-wordpress-1.0">PayPal.com</a>; or issue stamped and stamped electronic invoices with <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a>.
</p>
<p>
Currently the Organizations plugin loads a WordPress administration module located inside the <strong>Settings</strong> menu with the command name of <strong>Organizations</strong>. Which once invoked loads the module on the screen, as shown in <strong>Picture 1</strong>. Screen from which you can:
</p>
<p align="center">
<img src="%simages/organizations-list.png" alt="Picture 1 - Organizations of Galatea" style="width: 100%; height: auto;" />
Picture 1 - Organizations of Galatea
</p>
<p>
<ul>
<li>Create a new organization by clicking on the <strong>Add new</strong> link, located to the right of the module title.</li>
<li>Download the currently registered organizations to a new Microsoft Excel XLSX 2007 spreadsheet by clicking the <strong>Download</strong> link, located to the right of the module title.</li>
<li>Upload a set of registered organizations into a Microsoft Excel XLSX 2007 spreadsheet by pressing the <strong>Upload</strong> link, located to the right of the module title.</li>
<li>Modify the options on the screen by pressing the link <strong>Screen Options</strong>, located in the upper right corner of the module screen.</li>
<li>Query <strong>Help</strong> by pressing the link of the same name.</li>
<li>Query the organizations currently registered by clicking on the <strong>All</strong> link, located below the module title.</li>
<li>Check the deleted organizations by clicking on the <strong>Trash</strong> link, located below the module title.</li>
<li>Delete each of the registered organizations or all, pressing the <strong>Delete</strong> link located in the first column of the organizations table or selecting the organizations or applying the option of the same name within the <strong>Bulk Actions</strong>.</li>
<li>Recover each or all of the organizations deleted and not purged, by pressing the <strong>Recover</strong> link located in the first column of the organization table or by selecting the organizations or applying the option of the same name within the <strong>Bulk Actions</strong>.</li>
</ul>
</p>
<p align="center">
<img src="%simages/organizations-edit.png" alt="Picture 2 - Editing an organization" style="width: 100%; height: auto;" />
Picture 2 - Editing an organization
</p>
<p>
Now, when you create or edit an existing organization you must specify the following data for each organization:
</p>
<p>
<ul>
<li><strong>Email.</strong> Generally it is the main address used officially by this organization.</li>
<li><strong>Name.</strong> It is the common name of the organization. It can be a legal, commercial name or simply and simply the name used by the general public to identify this organization.</li>
<li><strong>Description.</strong> A brief description about this organization. It can be a short description about the organization, its mission and vision, main activities and services, etc.</li>
<li><strong>Main residence.</strong> It is the main residence of the organization. It is usually the place where the organization serves its customers.</li>
<li><strong>Federal taxpayer name.</strong> It is the legal name of the organization. Commonly registered before the governmental authorities.</li>
<li><strong>Federal taxpayer registration ID.</strong> Is the legal identification number of the organization. Commonly registered before the governmental authorities.</li>
<li><strong>Tax regime.</strong> It is the fiscal regime where the organization is assigned by the governmental authorities.</li>
<li><strong>Serial.</strong> It is a serial and alphanumeric number for all invoices issued by this organization through the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a>. It must be a unique number and distinct from other serial numbers used within other information systems used by the same organization.</li>
<li><strong>Currency code.</strong> It is the currency code used to issue all invoices through the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a> . It must be a valid currency code and registered into the <a href="https://es.wikipedia.org/wiki/ISO_4217">ISO 4217</a> according to the established in the specification guidelines of CFDIs version 3.3.</li>
<li><strong>Payment method.</strong> It is the payment method used by all customers of your online store and used inside of all issued invoices through our invoicing plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a> for your organization. Set the code of the payment method used to issue all invoices through the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a>. It must be a valid and registered payment method code according to those established in the specification guidelines of the CFDIs version 3.3.</li>
<li><strong>Use of the voucher.</strong> It is the use of the voucher used by all customers of your online store and whose invoices are issued through our invoicing plugin of <a href="#facturehoy.com">FacturaHoy.com </a> for your organization. It establishes the code for the use of the declared invoices issued through the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a>. It must be a valid and registered voucher use code according to those established in the specification guidelines of the CFDIs version 3.3.</li>
<li><strong>Digital certificate number.</strong> It is the digital certificate number declared within all invoices issued through the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a>. It must be a valid certificate number authorized and registered by government authorities.</li>
<li><strong>Electronic invoicing service URL.</strong> This is the URL of the electronic invoicing service system of <a href="https://facturehoy.com">https://facturehoy.com</a>, stamping and stamping services provider for electronic invoices, and through which the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a> will be connected.</li>
<li><strongUsername of the electronic invoicing service.</strong> It is the name of the user of the electronic invoicing service system of <a href="https://facturehoy.com">https://facturehoy.com</a>, stamping services provider and sealing for electronic invoices, and through which the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a> will be connected.</li>
<li><strong>Password of the user of the electronic invoicing service.</strong> This is the password of the electronic invoicing service system of <a href="https://facturehoy.com">https://facturehoy.com</a>, stamping services provider and sealing for electronic invoices, and through which the plugin of <a href="https://github.com/mrajuarez/facturehoy.com-wordpress-1.0">FactureHoy.com</a> will be connected.</li>
<li><strong>Type of electronic invoicing service.</strong> It is the available type of service of the system.</li>
<li><strong>Tax residence.</strong> It is the tax residence of the organization. It is commonly the place where the organization serves the government authorities.</li>
<li><strong>Main phones.</strong> Up to three main phone for the organizations. It is usually the phones where the organization serves its customers.</li>
<li><strong>Mobile.</strong> A mobile phone used by the organization, it this applies.</li>
<li><strong>Fax.</strong> A fax phone used by the organization, it this applies.</li>
<li><strong>Logo, photo or avatar.</strong> Generrally is used to identify this organization inside of Galatea\'s applications. For example, in Balanced Scorecard this file is showed in the views of every Key Performance Indicator and Perspective of this organization. We recommend an image file in the format of PNG (Portable Network Graphic) with 512 pixeles of width per 512 pixels of height.</li>
<li><strong>Invoice template.</strong> Contains an spreadsheet of Microsoft Excel XLSX 2007 with the invoices template used by this organization for issuing its own invoices. If you skip this template, the electronic invoicing services uses the default invoice template defined by your electronic invoicing services provider.</li>
</ul>
</p>
<p align="right">&copy; 2015-2017 Mario Rauz Alejandro Juárez Gutiérrez</p>
<p>
<img align="left" src="%simages/242.png" alt="242 Soluciones" style="width: auto; height: 100px;" />
<img align="right" src="%simages/GNU-GPLv3.png" alt="GNU-GPLv3" style="width: auto; height: 100px;" />
</p>', 'organizations'));
      return;
    } // function help_tab_organizations( ...
  } // if(!function_exists('help_tab_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('remove_logo')) {
    /**
     Remove a logo.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function remove_logo() {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      // Remove the logo of the selected record
      $sql = 'UPDATE gt_organizations SET
                logo = 0
              WHERE organization = "' . $_REQUEST['organizations'] . '"';
      $wpdb->query($sql);
      $sql = 'INSERT INTO gt_logs VALUES(
                "' . $current_user . '",
                DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                "event=The organization\'s logo has been removed.' .
               '&organization=' . $_REQUEST['organizations'] . '")';
      $wpdb->query ($sql);
      echo '<div id="message" class="updated notice is-dismissible">' .
             '<p>' . esc_html__('The organization\'s logo has been removed.', 'organizations') . '</p>' .
           '</div>';
      return;
    } // function remove_logo( ...
  } // if(!function_exists('remove_logo'))

  // Define the function only if this doesn't exists
  if(!function_exists('remove_template')) {
    /**
     Remove a template.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function remove_template() {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      // Remove the template of the selected record
      $sql = 'UPDATE gt_organizations SET
                template = 0
              WHERE organization = "' . $_REQUEST['organizations'] . '"';
      $wpdb->query($sql);
      $sql = 'INSERT INTO gt_logs VALUES(
                "' . $current_user . '",
                DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                "event=The organization\'s template has been removed.' .
               '&organization=' . $_REQUEST['organizations'] . '")';
      $wpdb->query ($sql);
      echo '<div id="message" class="updated notice is-dismissible">' .
             '<p>' . esc_html__('The organization\'s template has been removed.', 'organizations') . '</p>' .
           '</div>';
      return;
    } // function remove_template( ...
  } // if(!function_exists('remove_template'))

  // Define the function only if this doesn't exists
  if(!function_exists('restore_organizations')) {
    /**
     Restore a set of records.
     
     @param      array  $organizations  Organizations array emails.
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function restore_organizations($organizations) {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      for($i = 0; $i < count($organizations); $i++) {
        $sql = 'UPDATE gt_organizations SET
                  deleted = 0,
                  editor  = "' . $current_user . '",
                  edition = DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")
                WHERE organization = "' . $organizations[$i] . '"';
        $wpdb->query($sql);
        $sql = 'INSERT INTO gt_logs VALUES(
                  "' . $current_user . '",
                  DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                  "event=The selected organizations have been successfully restored.' .
                 '&organization=' . $organizations[$i] . '")';
        $wpdb->query ($sql);
      } // for($i = 0; $i < count($organizations); $i++)
      echo '<div id="message" class="updated notice is-dismissible">' .
             '<p>' . esc_html__('The selected organizations have been successfully restored.', 'organizations') . '</p>' .
           '</div>';
      return;
    } // function restore_organizations( ...
  } // if(!function_exists('restore_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('save_organization')) {
    /**
     Save a record.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function save_organization() {
      // Protect against unauthorized web entries
      if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('There was a serious unsafe access error to this script. Notify the administrator of this site immediately.', 'organizations') . '</p>' .
             '</div>';
        return;
      } // if(!wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      // Insert or update the record
      $sql = 'SELECT organization
              FROM gt_organizations
              WHERE organization = "' . $_REQUEST['organization'] . '"';
      $row = $wpdb->get_row($sql);
      if($wpdb->num_rows == 0 || is_null($row)) {
        $sql = 'INSERT INTO gt_organizations VALUES(
                  "' . $_REQUEST['organization'] . '",
                  "' . $_REQUEST['name'] . '",
                  "' . $_REQUEST['description'] . '",
                  "' . $_REQUEST['main_street'] . '",
                  "' . $_REQUEST['main_outside'] . '",
                  "' . $_REQUEST['main_inside'] . '",
                  "' . $_REQUEST['main_reference'] . '",
                  "' . $_REQUEST['main_settlement'] . '",
                  "' . $_REQUEST['main_postal_code'] . '",
                  "' . $_REQUEST['main_county'] . '",
                  "' . $_REQUEST['main_state'] . '",
                  "' . $_REQUEST['main_country'] . '",
                  "' . $_REQUEST['tax_name'] . '",
                  "' . $_REQUEST['tax_id'] . '",
                  "' . $_REQUEST['tax_regime'] . '",
                  "' . $_REQUEST['serial'] . '",
                  "' . $_REQUEST['currency'] . '",
                  "' . $_REQUEST['way_to_pay'] . '",
                  "' . $_REQUEST['payment_method'] . '",
                  "' . $_REQUEST['use_of_voucher'] . '",
                  "' . $_REQUEST['certificate_number'] . '",
                  "' . $_REQUEST['einvoicing_url'] . '",
                  "' . $_REQUEST['einvoicing_username'] . '",
                  "' . $_REQUEST['einvoicing_password'] . '",
                  "' . $_REQUEST['einvoicing_service'] . '",
                  "' . $_REQUEST['tax_street'] . '",
                  "' . $_REQUEST['tax_outside'] . '",
                  "' . $_REQUEST['tax_inside'] . '",
                  "' . $_REQUEST['tax_reference'] . '",
                  "' . $_REQUEST['tax_settlement'] . '",
                  "' . $_REQUEST['tax_postal_code'] . '",
                  "' . $_REQUEST['tax_county'] . '",
                  "' . $_REQUEST['tax_state'] . '",
                  "' . $_REQUEST['tax_country'] . '",
                  "' . (strlen($_REQUEST['phone_1']) > 25 ?
                         substr($_REQUEST['phone_1'], 0, 25) :
                                $_REQUEST['phone_1']) . '",
                  "' . (strlen($_REQUEST['phone_2']) > 25 ?
                         substr($_REQUEST['phone_2'], 0, 25) :
                                $_REQUEST['phone_2']) . '",
                  "' . (strlen($_REQUEST['phone_3']) > 25 ?
                         substr($_REQUEST['phone_3'], 0, 25) :
                                $_REQUEST['phone_3']) . '",
                  "' . (strlen($_REQUEST['mobile']) > 25 ?
                         substr($_REQUEST['mobile'], 0, 25) :
                                $_REQUEST['mobile']) . '",
                  "' . (strlen($_REQUEST['fax']) > 25 ?
                         substr($_REQUEST['fax'], 0, 25) :
                                $_REQUEST['fax']) . '",
                   ' . intval($_REQUEST['logo']) . ',
                   ' . intval($_REQUEST['template']) . ',
                  0,
                  "' . $current_user . '",
                  DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
        $wpdb->query($sql);
        $sql = 'INSERT INTO gt_logs VALUES(
                  "' . $current_user . '",
                  DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                  "event=The organization has been successfully created.' .
                 '&organization=' . $_REQUEST['organization'] . '")';
        $wpdb->query($sql);
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('The organization has been successfully created.', 'organizations') . '</p>' .
             '</div>';
      } else {
        $sql = 'UPDATE gt_organizations SET
                  name                = "' . $_REQUEST['name'] . '",
                  description         = "' . $_REQUEST['description'] . '",
                  main_street         = "' . $_REQUEST['main_street'] . '",
                  main_outside        = "' . $_REQUEST['main_outside'] . '",
                  main_inside         = "' . $_REQUEST['main_inside'] . '",
                  main_reference      = "' . $_REQUEST['main_reference'] . '",
                  main_settlement     = "' . $_REQUEST['main_settlement'] . '",
                  main_postal_code    = "' . $_REQUEST['main_postal_code'] . '",
                  main_county         = "' . $_REQUEST['main_county'] . '",
                  main_state          = "' . $_REQUEST['main_state'] . '",
                  main_country        = "' . $_REQUEST['main_country'] . '",
                  tax_name            = "' . $_REQUEST['tax_name'] . '",
                  tax_id              = "' . $_REQUEST['tax_id'] . '",
                  tax_regime          = "' . $_REQUEST['tax_regime'] . '",
                  serial              = "' . $_REQUEST['serial'] . '",
                  currency            = "' . $_REQUEST['currency'] . '",
                  way_to_pay          = "' . $_REQUEST['way_to_pay'] . '",
                  payment_method      = "' . $_REQUEST['payment_method'] . '",
                  use_of_voucher      = "' . $_REQUEST['use_of_voucher'] . '",
                  certificate_number  = "' . $_REQUEST['certificate_number'] . '",
                  einvoicing_url      = "' . $_REQUEST['einvoicing_url'] . '",
                  einvoicing_username = "' . $_REQUEST['einvoicing_username'] . '",
                  einvoicing_password = "' . $_REQUEST['einvoicing_password'] . '",
                  einvoicing_service  = "' . $_REQUEST['einvoicing_service'] . '",
                  tax_street          = "' . $_REQUEST['tax_street'] . '",
                  tax_outside         = "' . $_REQUEST['tax_outside'] . '",
                  tax_inside          = "' . $_REQUEST['tax_inside'] . '",
                  tax_reference       = "' . $_REQUEST['tax_reference'] . '",
                  tax_settlement      = "' . $_REQUEST['tax_settlement'] . '",
                  tax_postal_code     = "' . $_REQUEST['tax_postal_code'] . '",
                  tax_county          = "' . $_REQUEST['tax_county'] . '",
                  tax_state           = "' . $_REQUEST['tax_state'] . '",
                  tax_country         = "' . $_REQUEST['tax_country'] . '",
                  phone_1             = "' . (strlen($_REQUEST['phone_1']) > 25 ?
                                              substr($_REQUEST['phone_1'], 0, 25) :
                                              $_REQUEST['phone_1']) . '",
                  phone_2             = "' . (strlen($_REQUEST['phone_2']) > 25 ?
                                              substr($_REQUEST['phone_2'], 0, 25) :
                                              $_REQUEST['phone_2']) . '",
                  phone_3             = "' . (strlen($_REQUEST['phone_3']) > 25 ?
                                              substr($_REQUEST['phone_3'], 0, 25) :
                                              $_REQUEST['phone_3']) . '",
                  mobile              = "' . (strlen($_REQUEST['mobile']) > 25 ?
                                              substr($_REQUEST['mobile'], 0, 25) :
                                              $_REQUEST['mobile']) . '",
                  fax                 = "' . (strlen($_REQUEST['fax']) > 25 ?
                                              substr($_REQUEST['fax'], 0, 25) :
                                              $_REQUEST['fax']) . '",
                  logo                = ' . intval($_REQUEST['logo']) . ',
                  template            = ' . intval($_REQUEST['template']) . ',
                  deleted             = ' . intval($_REQUEST['deleted']) . ',
                  editor              = "' . $current_user . '",
                  edition             = DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")
                WHERE organization    = "' . $_REQUEST['organization'] . '"';
        $wpdb->query($sql);
        $sql = 'INSERT INTO gt_logs VALUES(
                  "' . $current_user . '",
                  DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                  "event=The organization has been successfully updated.' .
                 '&organization=' . $_REQUEST['organization'] . '")';
        $wpdb->query($sql);
        echo '<div id="message" class="updated notice is-dismissible">' .
               '<p>' . esc_html__('The organization has been successfully updated.', 'organizations') . '</p>' .
             '</div>';
      } // if($wpdb->num_rows == 0 || is_null($row))
      return;
    } // function save_organization( ...
  } // if(!function_exists('save_organization'))

  // Define the function only if this doesn't exists
  if(!function_exists('troubleshoting_tab_organizations')) {
    /**
     Print the Troubleshoting tab.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function troubleshoting_tab_organizations() { ?>
      <img src="<?php echo plugin_dir_url(__FILE__); ?>images/troubleshoting.jpg" alt="Troubleshoting" style="width: 100%; height: auto;" />
      <h2><?php esc_html_e('Troubleshoting', 'organizations') ?></h2>
      <p>
        <?php esc_html_e('Most of the time the records of this module work perfectly with the WordPress kernel and with the other plugins. Sometimes, there may be incompatibilities between some plugins causing problems, especially if they are made it by different programmmers. Your site could start doing weird things, which could be a problem. Try deactivating your plugins and activate them one by one; checking that the problem does not reappear. From this you can detect the plugin or the combination of problematic plugins.', 'organizations') ?>
      </p>
      <p>
        <?php
        printf(esc_html__('If something goes wrong with this plugin and you can not use your WordPress, delete or rename the file %s and it will be automatically deactivated. Also you can try deleting or renaming the database table of %s from your WordPress database.', 'organizations'), '<code>' . plugin_dir_path(__FILE__) . 'function.php</code>', '<code>gt_organizations</code>') ?>
      </p>
      <?php
      return;
    } // function troubleshoting_tab_organizations( ...
  } // if(!function_exists('troubleshoting_tab_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('upload_organizations')) {
    /**
     Upload a set of records.
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function upload_organizations() {
      // Loads the plugin utilities library
      include_once(ABSPATH . '/wp-admin/includes/plugin.php');

      // Protect against unauthorized web entries
      if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php') || !wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce')) {
        header('Status: 403 Forbidden');
        header('HTTP/1.1 403 Forbidden');
        wp_die();
      } // if(!is_plugin_active('phpspreadsheet-wordpress-1.0/functions.php') || !wp_verify_nonce($_REQUEST['_wpnonce'], 'organizations-nonce'))
      
      // Get de current Database WordPress connection
      global $wpdb;

      // Get the current user's email address
      $current_user = wp_get_current_user();
      $current_user = $current_user->user_email;

      // Open the uploaded file
      if(!is_uploaded_file($_FILES['uploaded_file']['tmp_name']))
        return;
      $uploads_path  = wp_upload_dir();
      $uploads_path  = $uploads_path['basedir'] . '/';
      if(file_exists($uploads_path . 'organizations-' . $current_user . '.xlsx'))
        unlink($uploads_path . 'organizations-' . $current_user . '.xlsx');
      if(!move_uploaded_file($_FILES['uploaded_file']['tmp_name'] , $uploads_path . 'organizations-' . $current_user . '.xlsx'))
        return;
      $spreadsheet = IOFactory::load($uploads_path . 'organizations-' . $current_user . '.xlsx');
      $sheet = $spreadsheet->getActiveSheet();
      $r = 2;
      while($sheet->getCell('A' . $r)->getValue() != '') {
        // Insert or update the record
        $sql = 'SELECT organization
                FROM gt_organizations
                WHERE organization = "' . $sheet->getCell('A' . $r)->getValue() . '"';
        $row = $wpdb->get_row($sql);
        if($wpdb->num_rows == 0 || is_null($row)) {
          $sql = 'INSERT INTO gt_organizations VALUES(
                   "' . $sheet->getCell('A' . $r)->getValue() . '",
                   "' . $sheet->getCell('B' . $r)->getValue() . '",
                   "' . $sheet->getCell('C' . $r)->getValue() . '",
                   "' . $sheet->getCell('D' . $r)->getValue() . '",
                   "' . $sheet->getCell('E' . $r)->getValue() . '",
                   "' . $sheet->getCell('F' . $r)->getValue() . '",
                   "' . $sheet->getCell('G' . $r)->getValue() . '",
                   "' . $sheet->getCell('H' . $r)->getValue() . '",
                   "' . $sheet->getCell('I' . $r)->getValue() . '",
                   "' . $sheet->getCell('J' . $r)->getValue() . '",
                   "' . $sheet->getCell('K' . $r)->getValue() . '",
                   "' . $sheet->getCell('L' . $r)->getValue() . '",
                   "' . $sheet->getCell('M' .  $r)->getValue() . '",
                   "' . $sheet->getCell('N' .  $r)->getValue() . '",
                   "' . $sheet->getCell('O' .  $r)->getValue() . '",
                   "' . $sheet->getCell('P' .  $r)->getValue() . '",
                   "' . $sheet->getCell('Q' .  $r)->getValue() . '",
                   "' . $sheet->getCell('R' .  $r)->getValue() . '",
                   "' . $sheet->getCell('S' .  $r)->getValue() . '",
                   "' . $sheet->getCell('T' .  $r)->getValue() . '",
                   "' . $sheet->getCell('U' .  $r)->getValue() . '",
                   "' . $sheet->getCell('V' .  $r)->getValue() . '",
                   "' . $sheet->getCell('W' .  $r)->getValue() . '",
                   "' . $sheet->getCell('X' .  $r)->getValue() . '",
                   "' . $sheet->getCell('Y' .  $r)->getValue() . '",
                   "' . $sheet->getCell('Z' .  $r)->getValue() . '",
                   "' . $sheet->getCell('AA' . $r)->getValue() . '",
                   "' . $sheet->getCell('AB' . $r)->getValue() . '",
                   "' . $sheet->getCell('AC' . $r)->getValue() . '",
                   "' . $sheet->getCell('AD' . $r)->getValue() . '",
                   "' . $sheet->getCell('AE' . $r)->getValue() . '",
                   "' . $sheet->getCell('AF' . $r)->getValue() . '",
                   "' . $sheet->getCell('AG' . $r)->getValue() . '",
                   "' . $sheet->getCell('AH' . $r)->getValue() . '",
                   "' . (strlen($sheet->getCell('AI' . $r)->getValue()) > 25 ?
                         substr($sheet->getCell('AI' . $r)->getValue(), 0, 25) :
                                $sheet->getCell('AI' . $r)->getValue()) . '",
                   "' . (strlen($sheet->getCell('AJ' . $r)->getValue()) > 25 ?
                         substr($sheet->getCell('AJ' . $r)->getValue(), 0, 25) :
                                $sheet->getCell('AJ' . $r)->getValue()) . '",
                   "' . (strlen($sheet->getCell('AK' . $r)->getValue()) > 25 ?
                         substr($sheet->getCell('AK' . $r)->getValue(), 0, 25) :
                                $sheet->getCell('AK' . $r)->getValue()) . '",
                   "' . (strlen($sheet->getCell('AL' . $r)->getValue()) > 25 ?
                         substr($sheet->getCell('AL' . $r)->getValue(), 0, 25) :
                                $sheet->getCell('AL' . $r)->getValue()) . '",
                   "' . (strlen($sheet->getCell('AM' . $r)->getValue()) > 25 ?
                         substr($sheet->getCell('AM' . $r)->getValue(), 0, 25) :
                                $sheet->getCell('AM' . $r)->getValue()) . '",
                   0,
                   0,
                   0,
                   "' . $current_user . '",
                   DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"))';
          $wpdb->query($sql);
          $sql = 'INSERT INTO gt_logs VALUES(
                   "' . $current_user . '",
                   DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                   "event=The organization has been successfully created.' .
                  '&organization=' . $sheet->getCell('A' . $r)->getValue() . '")';
          $wpdb->query($sql);
        } else {
          $sql = 'UPDATE gt_organizations SET
                    name                = "' . $sheet->getCell('B' .  $r)->getValue() . '",
                    description         = "' . $sheet->getCell('C' .  $r)->getValue() . '",
                    main_street         = "' . $sheet->getCell('D' .  $r)->getValue() . '",
                    main_outside        = "' . $sheet->getCell('E' .  $r)->getValue() . '",
                    main_inside         = "' . $sheet->getCell('F' .  $r)->getValue() . '",
                    main_reference      = "' . $sheet->getCell('G' .  $r)->getValue() . '",
                    main_settlement     = "' . $sheet->getCell('H' .  $r)->getValue() . '",
                    main_postal_code    = "' . $sheet->getCell('I' .  $r)->getValue() . '",
                    main_county         = "' . $sheet->getCell('J' .  $r)->getValue() . '",
                    main_state          = "' . $sheet->getCell('K' .  $r)->getValue() . '",
                    main_country        = "' . $sheet->getCell('L' .  $r)->getValue() . '",
                    tax_name            = "' . $sheet->getCell('M' .  $r)->getValue() . '",
                    tax_id              = "' . $sheet->getCell('N' .  $r)->getValue() . '",
                    tax_regime          = "' . $sheet->getCell('O' .  $r)->getValue() . '",
                    serial              = "' . $sheet->getCell('P' .  $r)->getValue() . '",
                    currency            = "' . $sheet->getCell('Q' .  $r)->getValue() . '",
                    way_to_pay          = "' . $sheet->getCell('R' .  $r)->getValue() . '",
                    payment_method      = "' . $sheet->getCell('S' .  $r)->getValue() . '",
                    use_of_voucher      = "' . $sheet->getCell('T' .  $r)->getValue() . '",
                    certificate_number  = "' . $sheet->getCell('U' .  $r)->getValue() . '",
                    einvoicing_url      = "' . $sheet->getCell('V' .  $r)->getValue() . '",
                    einvoicing_username = "' . $sheet->getCell('W' .  $r)->getValue() . '",
                    einvoicing_password = "' . $sheet->getCell('X' .  $r)->getValue() . '",
                    einvoicing_service  = "' . $sheet->getCell('Y' .  $r)->getValue() . '",
                    tax_street          = "' . $sheet->getCell('Z' .  $r)->getValue() . '",
                    tax_outside         = "' . $sheet->getCell('AA' . $r)->getValue() . '",
                    tax_inside          = "' . $sheet->getCell('AB' . $r)->getValue() . '",
                    tax_reference       = "' . $sheet->getCell('AC' . $r)->getValue() . '",
                    tax_settlement      = "' . $sheet->getCell('AD' . $r)->getValue() . '",
                    tax_postal_code     = "' . $sheet->getCell('AE' . $r)->getValue() . '",
                    tax_county          = "' . $sheet->getCell('AF' . $r)->getValue() . '",
                    tax_state           = "' . $sheet->getCell('AG' . $r)->getValue() . '",
                    tax_country         = "' . $sheet->getCell('AH' . $r)->getValue() . '",
                    phone_1             = "' . (strlen($sheet->getCell('AI' . $r)->getValue()) > 25 ?
                                                substr($sheet->getCell('AI' . $r)->getValue(), 0, 25) :
                                                       $sheet->getCell('AI' . $r)->getValue()) . '",
                    phone_2             = "' . (strlen($sheet->getCell('AJ' . $r)->getValue()) > 25 ?
                                                substr($sheet->getCell('AJ' . $r)->getValue(), 0, 25) :
                                                       $sheet->getCell('AJ' . $r)->getValue()) . '",
                    phone_3             = "' . (strlen($sheet->getCell('AK' . $r)->getValue()) > 25 ?
                                                substr($sheet->getCell('AK' . $r)->getValue(), 0, 25) :
                                                       $sheet->getCell('AK' . $r)->getValue()) . '",
                    mobile              = "' . (strlen($sheet->getCell('AL' . $r)->getValue()) > 25 ?
                                                substr($sheet->getCell('AL' . $r)->getValue(), 0, 25) :
                                                       $sheet->getCell('AL' . $r)->getValue()) . '",
                    fax                 = "' . (strlen($sheet->getCell('AM' . $r)->getValue()) > 25 ?
                                                substr($sheet->getCell('AM' . $r)->getValue(), 0, 25) :
                                                       $sheet->getCell('AM' . $r)->getValue()) . '",
                    deleted             = 0,
                    editor              = "' . $current_user . '",
                    edition             = DATE_FORMAT(NOW(), "%Y%m%d%H%i%s")
                  WHERE organization    = "' . $sheet->getCell('A' . $r)->getValue() . '"';
          $wpdb->query($sql);
          $sql = 'INSERT INTO gt_logs VALUES(
                    "' . $current_user . '",
                    DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                    "event=The organization has been successfully updated.' .
                   '&organization=' . $sheet->getCell('A' . $r)->getValue() . '")';
          $wpdb->query($sql);
        } // if($wpdb->num_rows == 0 || is_null($row))
        $r++;
      } // while($sheet->getCell('A' . $r)->getValue() != '')
      unset($spreadsheet);
      if(file_exists($uploads_path . 'organizations-' . $current_user . '.xlsx'))
        unlink($uploads_path . 'organizations-' . $current_user . '.xlsx');
      $sql = 'INSERT INTO gt_logs VALUES(
                "' . $current_user . '",
                DATE_FORMAT(NOW(), "%Y%m%d%H%i%s"),
                "event=The uploaded spreadsheet has been successfully processed.")';
      $wpdb->query($sql);
      echo '<div id="message" class="updated notice is-dismissible">' .
             '<p>' . esc_html__('The uploaded spreadsheet has been successfully processed.', 'organizations') . '</p>' .
           '</div>';
      return;
    } // function upload_organizations( ...
  } // if(!function_exists('upload_organizations'))

  // Define the function only if this doesn't exists
  if(!function_exists('update_postal_codes')) {
    add_action('wp_ajax_update_postal_codes', 'update_postal_codes');
    /**
     Update the settlements information inside of address constrols
     
     @author     Mario Rauz Alejandro Juárez Gutiérrez <mrajuarez@gmail.com>
     @since      1.0, Initial release
     */
    function update_postal_codes() {
      // Get teh current Database WordPress connection
      global $wpdb;

      // Get the corresponding set of postal codes
      $response = '';
      switch($_REQUEST['code']) {
        case 'states':
          $sql = 'SELECT DISTINCT state AS state
                  FROM gt_settlements
                  WHERE country = "' . $_REQUEST['country'] . '"
                  ORDER BY state';
          $results = $wpdb->get_results($sql);
          $i = 0;
          foreach($results as $row) {
            if($i != 0)
              $response .= ",";
            $response .= '{"key":"' . $row->state . '","name":"' . $row->state . '"}';
            $i++;
          } // foreach($results as $row)
          $response = "[" . $response . "]";
          header('Content-type: application/json');
          echo $response;
          wp_die();
          break;
        case 'counties':
          $sql = 'SELECT DISTINCT county AS county
                  FROM gt_settlements
                  WHERE country = "' . $_REQUEST['country'] . '" AND
                        state   = "' . $_REQUEST['state'] . '"
                  ORDER BY county';
          $results = $wpdb->get_results($sql);
          $i = 0;
          foreach($results as $row) {
            if($i != 0)
              $response .= ",";
            $response .= '{"key":"' . $row->county . '","name":"' . $row->county . '"}';
            $i++;
          } // foreach($results as $row)
          $response = "[" . $response . "]";
          header('Content-type: application/json');
          echo $response;
          wp_die();
          break;
        case 'settlements':
          $sql = 'SELECT DISTINCT settlement AS settlement
                  FROM gt_settlements
                  WHERE country = "' . $_REQUEST['country'] . '" AND
                        state   = "' . $_REQUEST['state'] . '" AND
                        county  = "' . $_REQUEST['county'] . '"
                  ORDER BY settlement';
          $results = $wpdb->get_results($sql);
          $i = 0;
          foreach($results as $row) {
            if($i != 0)
              $response .= ",";
            $response .= '{"key":"' . $row->settlement . '","name":"' . $row->settlement . '"}';
            $i++;
          } // foreach($results as $row)
          $response = "[" . $response . "]";
          header('Content-type: application/json');
          echo $response;
          wp_die();
          break;
        case 'postal_code':
          $sql = 'SELECT postal_code
                  FROM gt_settlements
                  WHERE country    = "' . $_REQUEST['country'] . '" AND
                        state      = "' . $_REQUEST['state'] . '" AND
                        county     = "' . $_REQUEST['county'] . '" AND
                        settlement = "' . $_REQUEST['settlement'] . '"
                  ORDER BY postal_code';
          $row = $wpdb->get_row($sql, ARRAY_A);
          if($wpdb->num_rows > 0 && !is_null($row))
            $response .= '{"value":"' . $row['postal_code'] . '"}';
          $response = "[" . $response . "]";
          header('Content-type: application/json');
          echo $response;
          wp_die();
          break;
        case 'address':
          $sql = 'SELECT state,
                         county,
                         settlement
                  FROM gt_settlements
                  WHERE country     = "' . $_REQUEST['country'] . '" AND
                        postal_code = "' . $_REQUEST['postal_code'] . '"
                  ORDER BY postal_code';
          $row = $wpdb->get_row($sql, ARRAY_A);
          if($wpdb->num_rows > 0 && !is_null($row))
            $response .= '{"state":"' . $row['state'] . '",' .
                          '"county":"' . $row['county'] . '",' .
                          '"settlement":"' . $row['settlement'] . '"}';
          $response = "[" . $response . "]";
          header('Content-type: application/json');
          echo $response;
          wp_die();
          break;
        default:
          // Protect against unauthorized web entries
          header('Status: 403 Forbidden');
          header('HTTP/1.1 403 Forbidden');
          wp_die();
          break;
      } // switch($_REQUEST['code'])
      wp_die();
    } // function update_postal_codes( ...
  } // if(!function_exists('update_postal_codes'))
} // if(is_admin())
// End of file